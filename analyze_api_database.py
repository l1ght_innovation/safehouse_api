import pymysql
import pymysql.cursors
import time
import datetime
import boto3
import logging
from botocore.exceptions import ClientError
import requests
import pandas as pd
import urllib.parse
import os
import json
import hashlib

rds_host = "localhost"
rds_name = 'admin'
rds_password = 'Antitoxin613'
rds_db_name = 'webscrapper'


def connect_db():
    conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def check_user(hashcode):
	conn = connect_db()
	cursor = conn.cursor()
	cursor.execute("select client_id from users where token='"+hashcode+"' LIMIT 1")
	result = cursor.fetchall()
	conn.close()
	if len(result) == 0:
		return -1
	else:
		return int(result[0]['client_id'])


def get_job_info(job_id):
	# Method to get all information on a particular job
	conn = connect_db()
	cursor = conn.cursor()
	cursor.execute("select * from jobs where job_id='" + job_id + "'")
	result = cursor.fetchall()
	conn.close()
	return result

def get_job_results(job_id):
	results = []
	conn = connect_db()
	cursor = conn.cursor()
	cursor.execute("select items.item_type, items.status, items.url, items.domain, items.text, items.imageid, items.imagename, items.data, results.errorcode, results.results from items left join results on items.job_id = results.job_id and items.id = results.item_id where items.job_id='" + job_id + "'")
	items = cursor.fetchall()
	urls = []
	texts = []
	images = []
	cleanresults = [{"label": "clean", "confidence": 1}]
	past_item_type = ''
	for item in items:
		errorcode = item['errorcode']
		if errorcode is None:
			errorcode = 0
		itemresults = item['results']
		if itemresults is None:
			itemresults = []
		else:
			itemresults = json.loads(itemresults.replace("'", '"'))
		if item['item_type'] == 'urls':
			if item['status']=='skipped':
				urls.append({'errorCode': errorcode, 'url': item['url'], 'results': 'item skipped'})
			elif itemresults == [] and errorcode == 0:
				urls.append({'errorCode': errorcode, 'url': item['url'], 'results': cleanresults})
			else:
				urls.append({'errorCode':errorcode, 'url': item['url'], 'results': itemresults})
		elif item['item_type'] == 'images':
			images.append({'errorCode':errorcode, 'imageId': item['imageid'], 'imageName': item['imagename'], 'results': itemresults})
		elif item['item_type'] == 'texts':
			texts.append({'errorCode':errorcode, 'text': item['text'], 'results': itemresults})
	conn.close()
	results = [{'images': images, 'siteClassification': urls, 'texts': texts}]
	return results

def check_job(job_id, client):
	# Method to return job information back
	result = get_job_info(job_id)
	if len(result)==0:
		return -1, 0,0,0
	else:
		clientid = result[0]['client_id']
		# Check if you are admin l1ght OR the valid client for the job
		if client!=1 and clientid!=client:
			return -9,0, 0, 0
		urls = result[0]['urls']
		if urls is None:
			urls = 0
		images = result[0]['images']
		if images is None:
			images = 0
		texts = result[0]['texts']
		if texts is None:
			texts = 0
		return result[0]['end'], images, urls, texts

def check_job_status(job_id):
	done=True
	conn=connect_db()
	cursor=conn.cursor()
	sql="select url,status from items where job_id='{}'".format(job_id)
	cursor.execute(sql)
	results=cursor.fetchall()
	for res in results:
		#print(res)
		if res['status']!='done' and res['status']!='skipped':
			done=False
			print('this item not finished:',res['url'],res['status'])
	return done

def save_item_results(conn, job_id, itemcode, errorcode, results):
	# Method to save toxic information about items (texts and images) from a job
	cursor = conn.cursor()
	sql = "select * from results where job_id='{}' and item_id={}".format(job_id, itemcode)
	cursor.execute(sql)
	item_results = cursor.fetchall()
	if len(item_results) > 0:
		# sql = "update results set results=%s where job_id=%s and item_id=%s"
		# val= (str(results),job_id,itemcode)
		sql="""update results set results="{}" where job_id='{}' and item_id={}""".format(str(results),job_id,itemcode)
		cursor.execute(sql)
	else:
		sql = "insert into results (job_id, item_id, results, errorcode) values (%s, %s, %s, %s)"
		val = (job_id, itemcode, str(results), errorcode)
		cursor.execute(sql, val)
	#print('save_item_results preformed this sql:', sql)
	conn.commit()
	return

def save_text_results(conn, job_id, term, itemcode, textfound):
	# Method to save toxic text results from a site to the texts table
	cursor = conn.cursor()
	sql = "insert into texts (job_id, item_id, term, text) values (%s, %s, %s, %s)"
	val = (job_id, itemcode, term, textfound)
	cursor.execute(sql, val)
	conn.commit()
	return



def save_image_results(conn, job_id, itemcode, label, filename, confidence, underage, time, type,results):
	# Method to save toxic image results from a site to the images table
	cursor = conn.cursor()
	sql = "insert into images (job_id, item_id, label, filename, confidence, underage, time, type,results) values (%s, %s, %s, %s, %s, %s, %s, %s,%s)"
	val = (job_id, itemcode, label, filename, confidence, underage, time, type,str(results).replace("'",'"'))
	#print('inserting this sql:',sql,val)
	cursor.execute(sql, val)
	conn.commit()
	return

def save_item_info(conn, job_id, item_type, url, domain, text, imageid, imagename, data, errorcode):
	# Method to save a new item (request) from a job to the items table
	cursor = conn.cursor()
	if data!='':
		datamd5 = hashlib.md5(data.encode())
		datastore = datamd5.hexdigest()
	else:
		datastore = ''
	sql = "insert into items (job_id, item_type,url, domain, text, imageid , imagename, data) values (%s, %s, %s, %s, %s, %s, %s, %s)"
	val = (job_id, item_type, url, domain, text, imageid, imagename, datastore)
	cursor.execute(sql, val)
	conn.commit()
	recordid = cursor.lastrowid
	if errorcode > 0:
		sql = "insert into results (job_id, item_id, results, errorcode) values (%s, %s, %s, %s)"
		val = (job_id, recordid, '{}', errorcode)
		cursor.execute(sql, val)
		conn.commit()
	return recordid

def get_hourly_quota(conn):
	cursor=conn.cursor()
	sql='select quota from configurations where type="hourly"'
	cursor.execute(sql)
	result=cursor.fetchall()
	quota=result[0]['quota']
	return quota

def get_jobs_from_past_hour(conn):
	cursor=conn.cursor()
	now=time.time()
	hour_ago=now-(60*60)
	sql='select count(*) from items where job_id in (select job_id from jobs where datereceived > {} and terminate is null) '.format(hour_ago)
	cursor.execute(sql)
	result=cursor.fetchall()
	#print('got these results from counter:' ,result)
	count=result[0]['count(*)']
	return count


def check_quota():
	conn=connect_db()
	hourly_quota=get_hourly_quota(conn)
	jobs_counter=get_jobs_from_past_hour(conn)
	conn.close()
	# if int(jobs_counter)<int(hourly_quota):
	# 	return True
	# else:
	# 	return False
	return True


def get_process_amount():
	conn=connect_db()
	cursor=conn.cursor()
	sql='select quota from configurations where type="process_amount"'
	cursor.execute(sql)
	result=cursor.fetchall()
	conn.close()
	return result[0]['quota']


def save_job_info(job_id, user, client, full_json):
	# Method to save a new job in the jobs table
	if 'datereceived' not in full_json:
		datereceived = time.time()
	else:
		datereceived = full_json['datereceived']
	if 'customerData' in full_json:
		customerdata = str(full_json['customerData'])
	else:
		customerdata = ""
	conn = connect_db()
	cursor = conn.cursor()
	ts = time.time()
	data = full_json['data']
	urls = 0
	texts = 0
	images = 0
	total_images = []
	total_urls = []
	total_texts = []

	for item in data:
		if 'images' in item:
			images += len(item['images'])
			for entry in item['images']:
				errorcode = 0
				if 'imageId' not in entry:
					imageid = ''
					errorcode = 500
				else:
					imageid = entry['imageId']
				if 'imageName' not in entry:
					imagename = ''
					errorcode = 500
				else:
					imagename = entry['imageName']
				if 'data' not in entry:
					data = ''
					errorcode = 500
				else:
					data = entry['data']
				item_code = save_item_info(conn, job_id, "images", "xx", 0, "uuu", imageid, imagename, data, errorcode)
				if errorcode == 0:
					total_images.append({"imageId": imageid, "imageName": imagename, "data": data, "itemcode": item_code})
		if 'siteClassification' in item:
			urls += len(item['siteClassification'])
			for entry in item['siteClassification']:
				errorcode = 0
				if 'url' not in entry:
					url = ''
					errorcode = 400
				else:
					url = entry['url']
				if 'domain' not in entry:
					domain = False
				else:
					domain = entry['domain']
				item_code = save_item_info(conn, job_id, 'urls', url, domain, '', '', '', '', errorcode )
				if errorcode == 0:
					total_urls.append({"url": url, "domain": domain, "itemcode": item_code})
		if 'texts' in item:
			texts += len(item['texts'])
			for entry in item['texts']:
				errorcode = 0
				if 'text' not in entry:
					text = ''
					errorcode = 300
				else:
					text = entry['text']
				item_code = save_item_info(conn, job_id, 'texts', '', 0, text, '', '', '', errorcode)
				if errorcode == 0:
					total_texts.append({"text": text, "itemcode": item_code})
	timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
	sql = "insert into jobs (job_id ,client_id, user_id, start, urls, texts, images, datereceived, customerdata) VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s)"
	val = (job_id,client, user, timestamp, urls, texts, images, datereceived, customerdata)
	cursor.execute(sql, val)
	conn.commit()
	conn.close()
	return total_images, total_urls, total_texts


def update_job_info(job_id):
	# Method to update the jobs table with the endtime of a job
	conn = connect_db()
	cursor = conn.cursor()
	ts = time.time()
	timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
	print('going to update job ',job_id)
	#cursor.execute("update jobs set end = '" + timestamp + "' where job_id='" + job_id + "'")
	cursor.execute("update jobs set end='{ts}' where job_id='{job_id}' ".format(ts=timestamp,job_id=job_id))
	conn.commit()
	conn.close()
	return


def get_item_status(itemcode):
	conn = connect_db()
	cursor = conn.cursor()
	sql = 'select status from items where id= "{}"'.format(itemcode)
	cursor.execute(sql)
	results=cursor.fetchall()
	status=results[0]['status']
	conn.close()
	return status

def update_item_status(itemcode, status):
	conn = connect_db()
	cursor = conn.cursor()
	sql = 'update items set status="'+ status+'" where id="'+str(itemcode)+'"'
	#print('SQL from update_item_status', sql)
	cursor.execute(sql)
	#print('sql above executed')
	conn.commit()
	conn.close()
	return

def update_item_results(job_id,item_id,results):
	conn=connect_db()
	cursor=conn.cursor()
	sql="update results set results='{}' where job_id='{}' and item_id={}".format(results,job_id,item_id)
	cursor.execute(sql)
	conn.commit()
	conn.close()
	return

def get_item_images(job_id,item_id,type='image'):
	conn=connect_db()
	cursor=conn.cursor()
	sql="select * from images where job_id='{job_id}' and item_id='{item_id}' and type='{type}'".format(job_id=job_id,item_id=item_id,type=type)
	#print('gonna exec this sql:', sql)
	cursor.execute(sql)
	results=cursor.fetchall()
	return results

def terminate_job(job_id):
	# Method to simulate termination of a job
	conn = connect_db()
	cursor = conn.cursor()
	cursor.execute("select * from jobs where job_id='" + job_id + "'")
	result = cursor.fetchall()
	if len(result) > 0:
		request_id = result[0]['request_id']
		cursor.execute("update jobs set terminate = True where job_id='" + job_id + "'")
		conn.commit()
	else:
		request_id = None
	conn.close()
	return request_id

def get_all_clients(conn):
    cursor = conn.cursor()
    cursor.execute("select * from clients")
    results = cursor.fetchall()
    return results

def get_all_users(conn):
    cursor = conn.cursor()
    cursor.execute("select * from users")
    results = cursor.fetchall()
    return results

def get_job_end_time(job_id):
	conn=connect_db()
	cursor=conn.cursor()
	sql='select end from jobs where job_id="{}"'.format(job_id)
	cursor.execute(sql)
	res=cursor.fetchall()
	end_time=res[0]['end']
	return end_time

def create_exempt_dict():
	key={}
	conn=connect_db()
	cursor=conn.cursor()
	sql="select * from exempt"
	cursor.execute(sql)
	results=cursor.fetchall()
	for res in results:
		key[res['domain']] = res['status']
	conn.close()
	return key

def get_item_results(job_id,item_id):
	conn=connect_db()
	cursor=conn.cursor()
	sql="select results from results where job_id='{}' and item_id={}".format(job_id,item_id)
	cursor.execute(sql)
	results=cursor.fetchall()
	item_result=results[-1]["results"]
	conn.close()
	return item_result

def get_all_items_from_url(url):
	print('getting all items for url',url)
	conn=connect_db()
	cursor=conn.cursor()
	sql="select * from items where url='{}'".format(url)
	cursor.execute(sql)
	results=cursor.fetchall()
	print('got these results',len(results))
	conn.close()
	return results

def update_skipped_url(url,item_results,errorcode):
	results=get_all_items_from_url(url)
	print('got these many items for url',url,len(results))
	conn=connect_db()
	cursor=conn.cursor()
	print('starting to update url results for ', url)
	for item in results:
		job_id=item['job_id']
		item_id=item['id']
		sql="select * from results where job_id='{}' and item_id={}".format(job_id,item_id)
		cursor.execute(sql)
		results=cursor.fetchall()
		if len(results)==0:
			#sql="insert into results (job_id,item_id,errorcode,results) values ('{}',{},0,'{}')".format(job_id,item_id,str(item_results))
			sql="insert into results (job_id,item_id,errorcode,results) values (%s,%s,%s,%s)"
			val=(job_id,item_id,errorcode,str(item_results))
		else:
			sql="update results set results=%s ,errorcode=%s where job_id=%s and item_id=%s"
			val=(str(item_results),errorcode,job_id,item_id)
		cursor.execute(sql,val)
		conn.commit()
	sql="update items set status='done' where url='{}'".format(url)
	cursor.execute(sql)
	conn.commit()
	conn.close()
	print('done updating url results for ', url)
	return


def get_sample_job(url):
	conn=connect_db()
	cursor=conn.cursor()
	sql="select * from items where url='{}' and status='skipped' limit 1".format(url)
	cursor.execute(sql)
	result=cursor.fetchall()
	job_id=result[0]['job_id']
	item_id=result[0]['id']
	domain=result[0]['domain']
	conn.close()
	return job_id,item_id,domain

def update_item_error(job_id,item_id,errorcode):
	conn=connect_db()
	cursor=conn.cursor()
	#see if item has existing result entry
	sql="select * from results where job_id='{}' and item_id={}".format(job_id,item_id)
	cursor.execute(sql)
	results=cursor.fetchall()
	if len(results)>0:
		#print('going to update error')
		sql="update results set errorcode={} where job_id='{}' and item_id={}".format(errorcode,job_id,item_id)
	else:
		#print('going to insert error')
		sql="insert into results (job_id,item_id,errorcode,results) values ('{}',{},{},'{}')".format(job_id,item_id,errorcode,str({}))
	cursor.execute(sql)
	conn.commit()
	#print('sql inserted', sql)
	conn.close()
	return

def get_sample_results(job_id,item_id):
	conn=connect_db()
	cursor=conn.cursor()
	sql="select * from results where job_id='{}' and item_id={}".format(job_id,item_id)
	cursor.execute(sql)
	results=cursor.fetchall()
	site_classification=results[0]['results']
	errorcode=results[0]['errorcode']
	conn.close()
	return site_classification,errorcode