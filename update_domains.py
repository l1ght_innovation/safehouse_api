import pymysql
import pymysql.cursors
import time
import datetime
import boto3
import logging
from botocore.exceptions import ClientError
import requests
import pandas as pd
import urllib.parse
import os
import json
import hashlib

rds_host = "localhost"
rds_name = 'admin'
rds_password = 'Antitoxin613'
rds_db_name = 'webscrapper'

def connect_db():
    conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def read_moderated_list(filename):
    linesin = pd.read_excel(filename)
    return linesin

def insert_domain_record(conn, domain, clean, labels, moderated, subdomain, type):
    cursor = conn.cursor()
    if type == 'insert':
        sql = "insert into domains (domain, clean, labels, moderated, subdomain) values (%s, %s, %s, %s, %s)"
        val = (domain, int(clean), labels, int(moderated), subdomain)
        cursor.execute(sql, val)
    else:
        labels = labels.replace("'",'"')
        sql = "update domains set clean = "+str(clean)+", labels = '"+labels+"', moderated = "+str(moderated)+" where domain='" + domain + "' and subdomain = '"+subdomain+"'"
        print(sql)
        cursor.execute(sql)
    conn.commit()


def update_domain(conn, domain, clean, labels, moderated, subdomain):
    cursor = conn.cursor()
    cursor.execute("select * from domains where domain='" + domain + "' and subdomain = '"+subdomain+"'")
    result = cursor.fetchall()
    if len(result) > 0:
        print("Found:", result)
        insert_domain_record(conn, domain, clean, labels, moderated, subdomain, 'update')
    else:
        insert_domain_record(conn, domain, clean, labels, moderated, subdomain, 'insert')


def process_changes(linesin, conn):
    for index, row in linesin.iterrows():
        domain = row['domain']
        clean = row['clean']
        labels = row['labels']
        moderated = row['moderated']
        subdomain = row['subdomain']
        print(domain, clean, labels, moderated, subdomain)
        update_domain(conn, domain, clean, labels, moderated, subdomain)

conn = connect_db()
filename = 'toxic-domains-moderated.xls'
linesin = read_moderated_list(filename)
process_changes(linesin,conn)
conn.close()
