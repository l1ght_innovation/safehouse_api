import base64
import json
import requests


def get_status_of_job(job_id):
    image_classification_url = 'http://safehouse.l1ght.com:4000/api/prod/v1/status'
    headers = {'content-type': 'application/json', 'Authentication': 'e5b60f39183c3e57c94c504784077fe4' }
    body = {"job_id":job_id}
    jsonbody = json.dumps(body)
    response = requests.post(image_classification_url, data=jsonbody, headers=headers)
    return response

def analyze_picture(imageid, imagename, imagedata):
    image_classification_url = 'http://safehouse.l1ght.com:4000/api/prod/v1/analyze'
    headers = {'content-type': 'application/json', 'Authentication': 'e5b60f39183c3e57c94c504784077fe4' }
    body = {"data":[{"images": [{"imageId":imageid, "imageName": imagename, "data":str(imagedata)}]}]}
    jsonbody = json.dumps(body)
    response = requests.post(image_classification_url, data=jsonbody, headers=headers)
    return response

def analyze_pictures(byte_array_of_pictures):
    image_classification_url = 'http://safehouse.l1ght.com:4000/api/prod/v1/analyze'
    headers = {'content-type': 'application/json', 'Authentication': 'e5b60f39183c3e57c94c504784077fe4' }
    image_array = []
    for image in byte_array_of_pictures:
        image_array.append({"imageId":'a', "imageName": 'b', "data":image})
    body = {"data":[{"images": image_array}]}
    jsonbody = json.dumps(body)
    response = requests.post(image_classification_url, data=jsonbody, headers=headers)
    return response

def get_image(fileName):
    with open(fileName, "rb") as image_file:
        data = base64.urlsafe_b64encode(image_file.read())
    image_file.close()
    return data.decode('utf-8')

def check_and_analyze(filename):
    #print("Getting image")
    data = get_image(filename)
    #print("Send to analyze")
    response = analyze_picture("1", "blah", data)
    statuscode = response.status_code
    if statuscode != 200:
        result = {'job_id': '', 'status': 'error', 'statuscode': statuscode}
        return result
    else:
        response_json = response.json()
        job_id = response_json['job_id']
        status = response_json['status']
        if status != 'in progress' and status != 'completed' and status != 'completed - results disabled':
            result = {'job_id': '', 'status': status, 'statuscode': statuscode}
            return
        else:
            completed = False
            while not completed:
                #print("Getting Status")
                response = get_status_of_job(job_id)
                statuscode = response.status_code
                if statuscode != 200:
                    result = {'job_id': '', 'status': 'error at status', 'statuscode': statuscode}
                    return result
                else:
                    #print("Checking Status")
                    response_json = response.json()
                    if 'message' in response_json:
                        if response_json['message'] == 'completed - results disabled':
                            #print('Done Restricted')
                            result = response_json
                            return result
                        #else:
                            #print('Still processing')
                    if 'status' in response_json:
                            #print('Done')
                            result = response_json
                            return result

def check_and_analyze_pictures(array_of_pictures):
    #print("Getting image")
    bytes_of_pictures = []
    for filename in array_of_pictures:
        data = get_image(filename)
        bytes_of_pictures.append(data)
    #print("Send to analyze")
    response = analyze_pictures(bytes_of_pictures)
    statuscode = response.status_code
    if statuscode != 200:
        result = {'job_id': '', 'status': 'error', 'statuscode': statuscode}
        return result
    else:
        response_json = response.json()
        job_id = response_json['job_id']
        status = response_json['status']
        if status != 'in progress' and status != 'completed' and status != 'completed - results disabled':
            result = {'job_id': '', 'status': status, 'statuscode': statuscode}
            return
        else:
            completed = False
            while not completed:
                #print("Getting Status")
                response = get_status_of_job(job_id)
                statuscode = response.status_code
                if statuscode != 200:
                    result = {'job_id': '', 'status': 'error at status', 'statuscode': statuscode}
                    return result
                else:
                    #print("Checking Status")
                    response_json = response.json()
                    if 'message' in response_json:
                        if response_json['message'] == 'completed - results disabled':
                            #print('Done Restricted')
                            result = response_json
                            return result
                        #else:
                            #print('Still processing')
                    if 'status' in response_json:
                            #print('Done')
                            result = response_json
                            return result


def test_one_image(filename, expected_label, expected_age):
    result = check_and_analyze(filename)
    if 'results' not in result:
        print("job error")
        print(result)
    images = result['results'][0]['images'][0]['results']
    sa_label=images[0]['label']
    if len(images)>1:
        ua_label=images[1]['label']
    else:
        ua_label = ''
    if sa_label == expected_label:
        if ua_label == expected_age:
            print("Test one image Passed", filename, images)
        else:
            print("Test one image Failed", filename, images)
    else:
        print("Test one image Failed", filename, images)

def test_multiple_image(array_of_images, array_of_expected_label, array_of_expected_age):
    result = check_and_analyze_pictures(array_of_images)
    if 'results' not in result:
        print("job error")
        print(result)
    print(result)

    images_list = result['results'][0]['images']
    count = 0
    for images_array in images_list:
        images = images_array['results']
        sa_label=images[0]['label']
        if len(images)>1:
            ua_label=images[1]['label']
        else:
            ua_label = ''
        if sa_label == array_of_expected_label[count]:
            if ua_label == array_of_expected_age[count]:
                print("Test multiple Passed", array_of_images[count], images)
            else:
                print("Test multiple Failed", array_of_images[count], images)
        else:
            print("Test multiple Failed", array_of_images[count], images)
        count+=1



filename = '/Users/johndoe/Desktop/flipjpg.jpg'
test_one_image(filename, 'sexual activity', 'underage')
filename = '/Users/johndoe/Desktop/aish-purim.png'
test_one_image(filename, 'clean', '')
filename = '/Users/johndoe/Desktop/michal.png'
test_one_image(filename, 'clean', '')
filename = '/Users/johndoe/Desktop/1.jpg'
test_one_image(filename, 'sexual activity', '')
filename = '/Users/johndoe/Desktop/2.jpg'
test_one_image(filename, 'sexual activity', '')
filename = '/Users/johndoe/Desktop/3.jpg'
test_one_image(filename, 'sexual activity', '')
filename = '/Users/johndoe/Desktop/4.jpg'
test_one_image(filename, 'sexual activity', '')
filename = '/Users/johndoe/Desktop/g1.jpg'
test_one_image(filename, 'sexual activity', '')
filename = '/Users/johndoe/Desktop/g2.jpg'
test_one_image(filename, 'sexual activity', '')
filename = '/Users/johndoe/Desktop/g3.jpg'
test_one_image(filename, 'sexual activity', '')
filename = '/Users/johndoe/Desktop/g4.jpg'
test_one_image(filename, 'sexual activity', '')
array_of_pictures = ['/Users/johndoe/Desktop/flipjpg.jpg', '/Users/johndoe/Desktop/aish-purim.png', '/Users/johndoe/Desktop/michal.png', '/Users/johndoe/Desktop/1.jpg',
                     '/Users/johndoe/Desktop/2.jpg', '/Users/johndoe/Desktop/3.jpg', '/Users/johndoe/Desktop/4.jpg', '/Users/johndoe/Desktop/g1.jpg',
                     '/Users/johndoe/Desktop/g2.jpg', '/Users/johndoe/Desktop/g3.jpg', '/Users/johndoe/Desktop/g4.jpg']
array_of_expected=['sexual activity','clean','clean','sexual activity','sexual activity','sexual activity','sexual activity','sexual activity','sexual activity','sexual activity','sexual activity']
array_of_ages=['','','','','','','','','','','']
test_multiple_image(array_of_pictures, array_of_expected, array_of_ages)