import pymysql
import pymysql.cursors
import shutil

rds_host = "localhost"
rds_name = 'admin'
rds_password = 'Antitoxin613'
rds_db_name = 'webscrapper'


def connect_db():
    conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def check_old_jobs():
    conn = connect_db()
    cursor = conn.cursor()
    sql = 'select job_id,start,(now()-start) as diff from jobs where end = "0000" and (now()-start) > 20000 order by start;'
    cursor.execute(sql)
    result = cursor.fetchall()
    conn.close()
    return result

def delete_old_job(job_id):
    shutil.rmtree(job_id, ignore_errors=True)
    return

def update_job_info(job_id):
    conn = connect_db()
    cursor = conn.cursor()
    sql = "update jobs set end = now() where job_id = '"+job_id+"'"
    cursor.execute(sql)
    result = cursor.fetchall()
    conn.commit()
    conn.close()
    return
    
print("Producing a list of old jobs")
results = check_old_jobs()
print('Results:', results)
for job in results: 
    job_id = job['job_id']
    print('job:', job_id)
    print('Deleting the job')
    delete_old_job(job_id)
    print('Updating the job')
    update_job_info(job_id)
print("Done")
