import tldextract
import pymysql
import pymysql.cursors
import time
import datetime
import boto3
import logging
from botocore.exceptions import ClientError
import requests
import pandas as pd
import urllib.parse
import os
import json
import hashlib

rds_host = "localhost"
rds_name = 'admin'
rds_password = 'Antitoxin613'
rds_db_name = 'webscrapper'


def connect_db():
    conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def extract_domain(url):
    url_details = tldextract.extract(url)
    base_url = url_details.domain + '.' + url_details.suffix
    #base_url = url_details.registered_domain
    #full_domain = url_details.fqdn
    subdomain = url_details.subdomain
    if subdomain =='':
        subdomain = "*"
    return subdomain, base_url

def save_new_domain(conn, domain, subdomain, clean, labels, moderated):
    cursor = conn.cursor()
    ts = time.time()
    dateupdated = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    #sql = "insert into domains (domain, subdomain, clean, labels, moderated, dateupdated) values (%s, %s, %d, %s, %d, %s)"
    sql = 'insert into domains (domain,subdomain,clean,labels,moderated, dateupdated) values ("{d}","{sd}",{c},"{l}",{m},"{date}")'.format(d=domain,sd=subdomain,c=clean,l=labels,m=moderated,date=dateupdated)
    #val = (domain, subdomain, clean, labels, moderated, dateupdated)
    #cursor.execute(sql, val)
    cursor.execute(sql)
    conn.commit()
    return

def update_existing_domain(conn, domain, subdomain, clean, labels, moderated):
    cursor = conn.cursor()
    ts = time.time()
    dateupdated = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    sql = "update domains set clean="+str(clean)+",labels='"+labels+"',moderated="+str(moderated)+",dateupdated='"+dateupdated+"' where domain='"+domain+"' and subdomain='"+subdomain+"'"
    cursor.execute(sql)
    conn.commit()
    return

def find_domain(conn, domain, subdomain):
    cursor = conn.cursor()
    try:
        cursor.execute("select * from domains where domain='" + domain + "' and subdomain='"+subdomain+"'")
        result = cursor.fetchall()
    except:
        result=[]
    return result

def process_url(conn, url, clean, labels, moderated):
    #print("Processing url...", url)
    subdomain, domain = extract_domain(url)
    #print("Found the domain....", domain)
    existing = find_domain(conn, domain, subdomain)
    if len(existing) == 0:
        print("adding in new domain", domain)
        save_new_domain(conn, domain, subdomain, clean, labels.lower(), moderated)
    else:
        print("updating existing domain", domain)
        update_existing_domain(conn, domain, subdomain, clean, labels.lower(), moderated)
    return

def process_file(filename, clean=1, labels="", moderated=1):
    conn = connect_db()
    print("Reading in the file", filename)
    linesin = pd.read_csv(filename)
    for index, row in linesin.iterrows():
        if len(row)==1:
            process_url(conn, row[0],clean,labels, moderated)
        else:
            process_url(conn, row[1], 0, row[6], 1)


#process_file('clean_from_sites_table.csv',1,"",1)
#process_file('clean_from_moderation_table.csv',1,"",1)
#process_file('deluxe_dirty_sites.csv',0,"",1)