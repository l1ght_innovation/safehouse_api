import os
import time
import datetime
import stat
import shutil
if __name__=="__main__":
    files=os.listdir('.')
    print(files)
    for file in files:
        if '363c' in file:
            print('checking file ', file)
            # st = os.stat(file)
            diff=time.time() - os.stat(file)[stat.ST_MTIME]
            print('diff is ', diff)
            # last_mod=os.path.getmtime(file)
            # print("last modified: %s" % time.ctime(os.path.getmtime(file)))
            if diff>10000:
                print('going to remove ',file)
                shutil.rmtree(file, ignore_errors=True)
                print('deleted ',file )