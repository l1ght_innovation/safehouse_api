import os
import pandas as pd
from bs4 import BeautifulSoup
from urllib.request import urlopen
import tldextract

def parse_one_file(page, site):
    soup = BeautifulSoup(page, 'html.parser')
    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.extract()  # rip it out
    # get images
    images = []
    soup_images = soup.find_all('img')
    for image in soup_images:
        try:
            i_src = image['src']
            extract = tldextract.extract(i_src)
            i_site = extract.domain + "." + extract.suffix
            if i_site.strip() != site.strip():
                images.append(i_src)
        except:
            pass
    # get links
    href_list = []
    soup_href_list = soup.find_all('a')
    for href in soup_href_list:
        try:
            h_src = href['href']
            if h_src.startswith('http'):
                extract = tldextract.extract(h_src)
                h_site = extract.domain + "." + extract.suffix
                if h_site.strip() != site.strip():
                    href_list.append(h_site)
        except:
            pass
    return images, href_list


def extract_href_from_file(filename, site):
    page = open(filename, "r", encoding="utf-8", errors='ignore')
    if page is None:
        print("Can't read url")
    else:
        images, href_list = parse_one_file(page, site)
    return images, href_list


def get_site_images_and_videos(job_id):
    images = []
    videos = []
    html = []
    for root, dir, files in os.walk(job_id):
            for file in files:
                ext = file.split('.')[-1].lower()
                if ext in ['jpg', 'jpeg', 'gif', 'webp', 'png', 'bmp']:
                    images.append({"filename": os.path.abspath(os.getcwd())+'/'+os.path.join(root, file)})
                elif ext in ['mp4', 'avi']:
                    videos.append(os.path.join(root, file))
                elif ext not in ['css', 'js', 'txt']:
                    html.append(os.path.join(root, file))
    return html, images, videos


def find_all_externals(job_id, site):
    htmls, images, videos = get_site_images_and_videos(job_id)
    images_list = []
    href_list = []
    for html_file in htmls:
        image_files, href_files = extract_href_from_file(html_file, site)
        for image in image_files:
            if image not in images_list:
                images_list.append(image)
        for href in href_files:
            if href not in href_list:
                href_list.append(href)
    return images_list, href_list

job_id = '/Users/johndoe/vpn/12345'
site = 'sex-escorts.co.za'
images_list, href_list = find_all_externals(job_id, site)
print(images_list, href_list)