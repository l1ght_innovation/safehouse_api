import analyze_api_database

def get_stats(conn):
    cursor=conn.cursor()
    for i in range(12,24):
        sql= "select count(*) from jobs where from_unixtime(datereceived) like '2020-03-25 {}:%' ".format(str(i))
        cursor.execute(sql)
        res_in=cursor.fetchall()
        print('hour:',i,'jobs recieved', res_in)
        sql= "select count(*) from jobs where end like '2020-03-25 {}:%' ".format(str(i))
        cursor.execute(sql)
        res_proc=cursor.fetchall()
        print('jobs processed:',res_proc )
        print('percent processed:', (float((res_proc[0]['count(*)']) / float(res_in[0]['count(*)']))) * 100)

    for i in range (0,11):
        time=str(i)
        if len(time)==1:
            time='0'+time
        sql = "select count(*) from jobs where from_unixtime(datereceived) like '2020-03-26 {}:%' ".format(time)
        cursor.execute(sql)
        res_in = cursor.fetchall()
        print('hour:', time, 'jobs recieved', res_in)
        sql = "select count(*) from jobs where end like '2020-03-26 {}:%' ".format(str(time))
        cursor.execute(sql)
        res_proc = cursor.fetchall()
        print('jobs processed:', res_proc)
        print('percent processed:', (float((res_proc[0]['count(*)']) / float(res_in[0]['count(*)']))) * 100)


if __name__=="__main__":
    conn=analyze_api_database.connect_db()
    get_stats(conn)