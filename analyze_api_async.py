from celery import Celery, Task
import asyncio
from codetiming import Timer
import glob
import json
import time
import subprocess
import requests
import shutil
import os
import analyze_api_database
import cv2
import tldextract
import upload_domains
import check_external_links
import uuid
import random

app = Celery('analyze_api_async',
             backend='rpc://',
             broker='amqp://guest:guest@localhost:5672//')

@app.task(bind=True)
def celery_process_analyze_prod_start(self, user, job_id, data, total_images, total_urls, total_texts):
#   Process Analyze API
# Get the total amount to process -- right now set as 1/10
    process_amount = analyze_api_database.get_process_amount()
    process_analyze_prod_api(user, job_id, data, total_images, total_urls, total_texts, process_amount)
    return

@app.task(bind=True)
def celery_process_stop_job(request_id):
    if request_id is not None:
        app.control.revoke(request_id)
    return

def analyze_text(text_dict):
#   Analyze the list of text (DATA) that are sent
    text_classification_url = 'http://0.0.0.0:8031/api/classify'
    headers = {'content-type': 'application/json'}
    body = {"conversation": text_dict}
    jsonbody = json.dumps(body)
    response = requests.post(text_classification_url, data=jsonbody, headers=headers)
    return response

def analyze_picture(images_dict):
#   Analyze the list of images (DATA) that are sent
    image_classification_url = 'http://0.0.0.0:7029/api/data/classify'
    headers = {'content-type': 'application/json'}
    body = {"images": images_dict}
    jsonbody = json.dumps(body)
    response = requests.post(image_classification_url, data=jsonbody, headers=headers)
    return response

def analyze_file_picture(images_dict):
#   Analyze the list of images (FILES) that are sent
    port=random.choice(range(0,10))
    image_classification_url = 'http://0.0.0.0:7'+str(port)+'29/api/classify'
    headers = {'content-type': 'application/json'}
    body = {"images": images_dict}
    jsonbody = json.dumps(body)
    #print('sending this request to image processing: ' ,jsonbody)
    response = requests.post(image_classification_url, data=jsonbody, headers=headers)
    #print('got back this response from image model:' ,response)
    return response

def frame_video(directory, path, filename, time=1000):
    vidcap = cv2.VideoCapture(path)
    numframes = vidcap.get(cv2.CAP_PROP_FRAME_COUNT)
    fps =  int(vidcap.get(cv2.CAP_PROP_FPS) + .5)
    totalcount = int(numframes /  fps * (time/1000))
    success, image = vidcap.read()
    count = 0
    while success and count <totalcount:
        cv2.imwrite(directory + "/"+filename+"--%d.jpg" % count, image)  # save frame as JPEG file
        vidcap.set(cv2.CAP_PROP_POS_MSEC, (count * time))
        success, image = vidcap.read()
        #print('Read a new frame: ', success)
        count += 1
    return

def download_site(job_id, url, levels, itemcode):
#   Use WGET and Download a Site
    print("Downloading Site:  ", url)
    if levels != "":
        process = subprocess.run(
            ['timeout', '10m', 'wget2', '--directory-prefix=' + job_id+ '/'+str(itemcode),'--output-file=wget.log', ' --page-requisites', '--no-parent', '--tries=1',
             '--read-timeout=5', '--waitretry=1', '--no-check-certificate', '--recursive', '-l=' + levels, url],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    else:
        process = subprocess.run(
            ['timeout', '5m', 'wget2', '--directory-prefix=' + job_id+ '/'+str(itemcode), ' --page-requisites', '--no-parent', '--tries=1',
             '--read-timeout=5', '--waitretry=1', '--no-check-certificate',  '--no-dns-cache',url],
        stdout = subprocess.PIPE,
                 stderr = subprocess.PIPE)
    errorcode = process.returncode
    return errorcode


def download_site_non_recursive(job_id, url, levels, itemcode):
#   Use WGET and Download a Site
    print("Downloading Site:  ", url)
    if levels != "":
        process = subprocess.run(
            ['timeout', '10m', 'wget2', '--directory-prefix=' + job_id+ '/'+str(itemcode),'--output-file=wget.log', ' --page-requisites', '--no-parent', '--tries=1',
             '--read-timeout=5', '--waitretry=1', '--no-check-certificate', '-l=' + levels, url],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    else:
        process = subprocess.run(
            ['timeout', '5m', 'wget2', '--directory-prefix=' + job_id+ '/'+str(itemcode), ' --page-requisites', '--no-parent', '--tries=1',
             '--read-timeout=5', '--waitretry=1', '--no-check-certificate',  '--no-dns-cache',url],
        stdout = subprocess.PIPE,
                 stderr = subprocess.PIPE)
    errorcode = process.returncode
    return errorcode


def grep_sites(job_id,itemcode):
    print("Checking the text on all the sites")
    process = subprocess.run(
       ['./grep_all.sh', job_id,itemcode],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE, timeout=1000)
    errorcode = process.returncode
    return errorcode

def get_site_images_and_videos(job_id):
    images = []
    videos = []
    for root, dir, files in os.walk(job_id):
            for file in files:
                ext = file.split('.')[-1].lower()
                if ext in ['jpg', 'jpeg', 'gif', 'webp', 'png', 'bmp']:
                    images.append({"filename": os.path.abspath(os.getcwd())+'/'+os.path.join(root, file)})
                if ext in ['mp4', 'avi']:
                    videos.append(os.path.join(root, file))
    return images, videos

def analyze_image_results(json_results):
    results = []
    for item in json_results:
        filename = item['filename']
        image_results = {}
        prediction = item['predictions']
        # 1 sexual activity
        if prediction[0]['sexual_activity'] > .5:
            image_results["sexual_activity"] = prediction[0]['sexual_activity']
        elif prediction[0]['nudity'] > .5:
            image_results["nudity"] = prediction[0]['nudity']
        else:
            image_results["clean"] = max(prediction[0]['erotica'], prediction[0]['neutral'])
        # 2 is age
        image_results["underage"] = 0
        if isinstance(prediction[1]['value'], str):
            if prediction[1]['value'] == 'child' or prediction[1]['value'] == 'teen':
                image_results["underage"] = 1
        elif isinstance(prediction[1]['value'], list):
            for person in prediction[1]['value']:
                if (person['value'] == 'child' or person['value'] == 'teen') and person['confidence']>0.8:
                    image_results["underage"] = 1
                    break
        # 3 is hate
        image_results['hate'] = prediction[2]['hate']
        results.append({"filename": filename, "image_results": image_results})
    return results

def analyze_site_images(job_id, images):
    print("Checking images on the sites")
    conn = analyze_api_database.connect_db()
    timer = Timer(text=f"Site Images Task elapsed time: {{:.1f}}")
    timer.start()
    #print('sending this to image endpoint: ', images)
    response = analyze_file_picture(images)
    #print('got this response from image endpoint ', response)
    if response.status_code == 200:
        print('got this back from the model:',response.json())
        results = analyze_image_results(response.json()['images'])
        #print('after analyzing results:',results)
        for result in results:
                filename = result['filename']
                filename_full = filename.split('/')
                itemcode = filename_full[5]
                # underage = result['underage']
                # label = result['label']
                # confidence = result['confidence']
                # clean = float(result['clean'])
                underage = None
                label = None
                confidence = None
                clean = None
                site_filename = ('/').join(filename_full[6:])
                image_results=result['image_results']
                analyze_api_database.save_image_results(conn, job_id, itemcode, label, site_filename, confidence, underage, 0, 'image',image_results)
    timer.stop()
    conn.close()
    return

def frame_video(directory, path, filename, time=1000):
    vidcap = cv2.VideoCapture(path)
    numframes = vidcap.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = int(vidcap.get(cv2.CAP_PROP_FPS) + .5)
    totalcount = int(numframes / fps * (time / 1000))
    success, image = vidcap.read()
    count = 0
    while success and count < totalcount:
        cv2.imwrite(directory + "/" + filename + "--%d.jpg" % count, image)  # save frame as JPEG file
        vidcap.set(cv2.CAP_PROP_POS_MSEC, (count * time))
        success, image = vidcap.read()
        # print('Read a new frame: ', success)
        count += 1

def video_to_frames(path, root='/home/centos/analyze_api'):
    conn=analyze_api_database.connect_db()
    path_arr = path.split('/')
    #print(path_arr)
    job_id = path_arr[0]
    directory = '/'.join(path_arr[:-1])
    vid_name = path_arr[-1].split('.')[0]
    # create directory
    new_vid_directory = os.path.join(directory, vid_name)
    if not os.path.isdir(new_vid_directory):
        os.makedirs(new_vid_directory)
    else:
        shutil.rmtree(new_vid_directory, ignore_errors=True)
        os.makedirs(new_vid_directory)
    try:
        frame_video(new_vid_directory, path, vid_name)
    except Exception as e:
        print('could not parse frame from video: ', path, 'got this error: ', e)
        # send all files in that directory to image analysis
    images_from_video = os.listdir(new_vid_directory)
    images=[]
    for file in images_from_video:
        images.append({"filename": os.path.join(root, new_vid_directory, file)})
    #print('sending this request: ', images)
    results_predicted=analyze_file_picture(images)
    # endpoint = webscrapper.calculate_endpoint(path)
    # results_predicted = picture_classification.new_analyze_picture_local(request_body, endpoint)
    try:
        res_json = results_predicted.json()
        #print('got these results in video_to_frames',res_json)
        results = analyze_image_results(res_json['images'])
        #print('results after analyzing:',results)
        for result in results:
            filename = result['filename']
            filename_full = filename.split('/')
            itemcode = filename_full[5]
            underage = result['underage']
            label = result['label']
            confidence = result['confidence']
            clean = float(result['clean'])
            site_filename = ('/').join(filename_full[6:])
            if clean == 0:
                #print('going to insert to images:', job_id, itemcode, label, site_filename, confidence, underage, 0,'video')
                analyze_api_database.save_image_results(conn, job_id, itemcode, label, site_filename, confidence,
                                                        underage, 0, 'video')
    except Exception as e:
        print('got this error in video_to_frames:', e)
    conn.close()


def analyze_site_videos(job_id, videos):
    print("Checking videos on the sites")
    #print("got these videos:",videos)
    for video in videos:
        video_to_frames(video)
    conn = analyze_api_database.connect_db()
    conn.close()
    return

def analyze_grep_results(job_id):
    offset=0
    print("Analyzing the results of the text on all the sites")
    conn = analyze_api_database.connect_db()
    logfiles = glob.glob('./'+job_id+"/*.log")
    porn = {}
    escort = {}
    for logfile in logfiles:
        file = open(logfile, 'r', encoding='utf-8', errors='ignore')
        base_filename = logfile.split('/')[-1]
        term = base_filename.split('.')[0]
        #print("Analyzing "+term+" hits")
        lines = file.readlines()
        for line in lines:
            #print('checking this line: ',line)
            linesplit = line.split(':')
            urlsplit = linesplit[0].split('/')
            #print('this is the url split ', urlsplit)
            if len(urlsplit)<=(offset+5):
                continue
            else:
                itemcode = urlsplit[5+offset]
                textfound=':'.join(linesplit[1:])
                index=textfound.find(term)
                textfound=textfound[max(0,index-124):min(index+125,len(textfound))].strip()
                try:
                    analyze_api_database.save_text_results(conn, job_id, term, int(itemcode), textfound)
                except:
                    continue
                if term == 'escort' or term == 'escorts':
                    if itemcode in escort:
                        escort[itemcode] = escort[itemcode] + 1
                    else:
                        escort[itemcode] = 1
                else:
                    if itemcode in porn:
                        porn[itemcode] = porn[itemcode] + 1
                    else:
                        porn[itemcode] = 1
    # for key in porn:
    #     if porn[key]>3:
    #         if key in escort:
    #             if escort[key]>3:
    #                 # update the domain table also
    #                 analyze_api_database.save_item_results(conn, job_id, key, 0,[{"label": "porn", "confidence": 1}, {"label": "escort", "confidence": 1}])
    #             else:
    #                 # update the domain table also
    #                 analyze_api_database.save_item_results(conn, job_id, key, 0,[{"label": "porn", "confidence": 1}])
    #         else:
    #             # update the domain table also
    #             try:
    #                 analyze_api_database.save_item_results(conn, job_id, key, 0, [{"label": "porn", "confidence": 1}])
    #             except:
    #                 continue
    # for key in escort:
    #     if escort[key]>3:
    #         if key in porn:
    #             if porn[key]<=3:
    #                 # update the domain table also
    #                 try:
    #                     analyze_api_database.save_item_results(conn, job_id, key, 0, [{"label": "escort", "confidence": 1}])
    #                 except:
    #                     continue
    #         else:
    #             # update the domain table also
    #             try:
    #                 analyze_api_database.save_item_results(conn, job_id, key, 0, [{"label": "escort", "confidence": 1}])
    #             except:
    #                 continue
    conn.close()
    return

async def check_images_task(data, job_id, process_amount):
    # Check images given
    conn = analyze_api_database.connect_db()
    timer = Timer(text=f"Images Task elapsed time: {{:.1f}}")
    timer.start()
    response = analyze_picture(data)
    #print('got this response from image endpoint: ', response,response.json())
    data_results = response.json()['images']
    count = 0
    for item in data_results:
        itemcode = item['itemcode']
        if count % process_amount == 0:
            item_results = item['predictions']
            results = []
            for result in item_results:
                value = result['value']
                rlabel = result['label']
                if rlabel == 'sexual_activity':
                    if value == 'erotica' or value == 'neutral':
                        value = 'clean'
                if rlabel == 'hate':
                    if value == 'neutral':
                        value = 'no hate'
                if type(value) is list:
                    # This is the age classifier
                    label = "age"
                    confidence = 0
                    for v in value:
                        if v['value'] == 'under_age_18':
                            label = "underage"
                            confidence = max(confidence, v['confidence'])
                    if label == 'underage':
                        results.append({"confidence": str(confidence), "label": label})
                else:
                    results.append({"confidence": result['confidence'], "label": value})
            if len(results) == 0:
                errorcode = 500
            else:
                errorcode = 0
            analyze_api_database.save_item_results(conn, job_id, itemcode, errorcode, results)
        else:
            analyze_api_database.update_item_status(itemcode, 'skipped')
        count+=1
    timer.stop()
    conn.close()
    return

def get_site_classification(job_id,itemcode):
    conn=analyze_api_database.connect_db()
    cursor=conn.cursor()
    #get text results
    sql="select * from texts where job_id='{job_id}' and item_id='{itemcode}'".format(job_id=job_id,itemcode=itemcode)
    cursor.execute(sql)
    text_results=cursor.fetchall()
    #count escort hits
    escort_count=0
    for res in text_results:
        if 'escort' in res['term']:
            escort_count+=1
    #get image results
    sql = "select * from images where job_id='{job_id}' and item_id='{itemcode}' and type='image'".format(job_id=job_id, itemcode=itemcode)
    cursor.execute(sql)
    image_results = cursor.fetchall()
    dirty_images=0
    for image in image_results:
        if any(porn_indicator in image['results'] for porn_indicator in ['sexual_activity','nudity']):
            dirty_images+=1
    #decide classification
    if dirty_images+len(text_results)>2:
        site_classification='porn'
        if escort_count>2:
            site_classification='escort'
    else:
        site_classification='clean'
    site_classification=[{"label": site_classification, "confidence" : 1}]
    return site_classification


def update_results(job_id,data):
    print('updating results with this data',data)
    conn = analyze_api_database.connect_db()
    cursor = conn.cursor()
    for item in data:
        item_status=analyze_api_database.get_item_status(item['itemcode'])
        #print('got this item status ',item_status,'for this item',item)
        if not item_status=='done':
            continue
        url = item['url'].strip()
        url_type=get_url_type(url)
        #print('this is the url type ',url,url_type)
        subdomain,base_url=upload_domains.extract_domain(url)
        res=upload_domains.find_domain(conn,base_url,subdomain)
        if len(res) == 0 and subdomain != '':
            res = upload_domains.find_domain(conn, base_url, '*')
        #if the url is from a known dirty domain, update the item's results with the domain's labels
        if len(res)>0 and res[0]['labels']!='[]' and res[0]['moderated']==1 :
            site_classification=res[0]['labels']
            analyze_api_database.save_item_results(conn, job_id,item['itemcode'],0,site_classification)
        elif any(porn_indicator in base_url.lower() for porn_indicator in ['porn', 'xnxx', 'xvideos']):
            # label as porn, save in domains table
            site_classification='[{"label": "porn", "confidence": 1}]'
            analyze_api_database.save_item_results(conn, job_id, item['itemcode'], 0, site_classification)
            upload_domains.save_new_domain(conn, base_url, subdomain, 0,"[{'label': 'porn', 'confidence': 1}]" , 0)
        else:
            #if it's an image from a clean domain, label the result according to the label in images table for that image
            if url_type=='image':
                item_images=analyze_api_database.get_item_images(job_id,item['itemcode'])
                print('got these image results',job_id,item['itemcode'],item_images,type(item_images),len(item_images))
                if len(item_images)==0:
                    image_results = {"messege": "could not analyze image"}
                else:
                    image_results = item_images[0]['results']
                # if len(item_images)>0:
                #     image_results=item_images[0]['results']
                # else:
                #     image_results={"messege":"could not analyze image"}
                site_classification=image_results
                print('this is the site classification',site_classification)
                analyze_api_database.save_item_results(conn, job_id, item['itemcode'], 0,site_classification)
            elif url_type=='video':
                item_images = analyze_api_database.get_item_images(job_id, item['itemcode'],type='video')
                if len(item_images) > 10:
                    site_classification='[{"label": "porn", "confidence": 1}]'
                    analyze_api_database.save_item_results(conn, job_id, item['itemcode'], 0,
                                                           '[{"label": "porn", "confidence": 1}]')
                else:
                    site_classification='[{"label": "clean", "confidence": 1}]'
                    analyze_api_database.save_item_results(conn, job_id, item['itemcode'], 0,
                                                           '[{"label": "clean", "confidence": 1}]')
            else:
                site_classification=get_site_classification(job_id,item['itemcode'])
                analyze_api_database.save_item_results(conn, job_id, item['itemcode'], 0, site_classification)
                if url_type=='domain':
                    if site_classification[0]["label"]=='clean':
                        clean=1
                    else:
                        clean=0
                    upload_domains.save_new_domain(conn,base_url,subdomain, clean,site_classification,0)


        # else:
        #     sql = "select * from results where item_id='{}'".format(item['itemcode'])
        #     cursor.execute(sql)
        #     results = cursor.fetchall()
        #     if len(results) == 0:
        #         clean = 1
        #         labels = []
        #         analyze_api_database.save_item_results(conn, job_id, item['itemcode'], 0, labels)
        #     else:
        #         clean = 0
        #         labels = results[0]['results']
        #     print('going to save item results:', job_id, item['itemcode'], labels)
        #     # analyze_api_database.save_item_results(conn, job_id, item['itemcode'], 0, labels)
        #
        #     if url[-1] == '/':
        #         url = url[:-1]
        #     if url.startswith('http'):
        #         index = url.index('//')
        #         url = url[index + 2:]
        #     subdomain, base_url = upload_domains.extract_domain(url)
        #     print('comparing these::::', 'url', url, 'sd', subdomain, 'bu', base_url)
        #     if url == base_url or url == subdomain + '.' + base_url:
        #         try:
        #             upload_domains.save_new_domain(conn, base_url, subdomain, clean, labels, 0)
        #         except:
        #             print('failed uploading to domains table: ', base_url, subdomain)
    conn.close()
    return site_classification


def get_url_type(url):
    image_ext=['.jpeg','.jpg','.bmp','.gif','.png','.webp']
    vid_ext=['.mov','.mp4','.avi']
    url=url.lower()
    if url.startswith('http'):
        index=url.find('//')+2
        url=url[index:]
    if url[-1]=='/':
        url=url[:-1]
    subdomain, base_url=upload_domains.extract_domain(url)
    if url==base_url or url==subdomain+'.'+base_url:
        type='domain'
    elif any(ext in url for ext in image_ext):
        type='image'
    elif any(ext in url for ext in vid_ext):
        type='video'
    else:
        type='page'
    return type


@app.task(bind=True)
def check_a_site(self,item,job_id,data,skipped=False):
    print("Running Check on item ", item['url'])
    #job_id = str(uuid.uuid1())
    url = item['url'].strip()
    if url[-1]=='?':
        url=url[:-1]
    if 'domain' in item:
        domain = item['domain']
    else:
        domain = False
    if domain:
        levels = '1'
    else:
        levels = '1'
    url_type=get_url_type(url)
    subdomain, base_url = upload_domains.extract_domain(url)
    conn = analyze_api_database.connect_db()
    results = upload_domains.find_domain(conn, base_url, subdomain)
    if len(results) == 0 and subdomain != '':
        results = upload_domains.find_domain(conn, base_url, '*')
    if any(porn_indicator in base_url for porn_indicator in ['porn','xnxx','xvideos']):
        #do nothing
        print('page url with porn indicator')
    elif url_type=='domain' and len(results)>0:
        #do nothing
        print('url found in domains table')
    elif (url_type=='image' or url_type=='video') and len(results)>0 and results[0]['labels']!='[]' and results[0]['moderated']==1:
        print('image from toxic domain found', url)
    elif url_type=='page' and len(results)>0 and results[0]['labels']!='[]' and results[0]['moderated']==1:
        print('found page from toxic domain')
    else:
        if url_type=='image' or url_type=='video':
            errorcode=download_site_non_recursive(job_id,url,levels,item['itemcode'])
        else:
            errorcode = download_site(job_id, url, levels, item['itemcode'])
        print('got this error code after download site: ', errorcode)
        if not errorcode==0:
            print('going to update error code for ',job_id,item['itemcode'])
            analyze_api_database.update_item_error(job_id,item['itemcode'],errorcode)
        errorcode = grep_sites(job_id,str(item['itemcode']))
        print('done grepping with this err:' ,errorcode)
        # get external link and images
        external_images, external_hrefs = check_external_links.find_all_externals(job_id, str(item['itemcode']),
                                                                                  url)
        # download external images
        check_external_links.download_external_images(external_images, job_id, item['itemcode'])
        for link in external_hrefs:
            subdomain_ex, base_url_ex = upload_domains.extract_domain(link)
            conn = analyze_api_database.connect_db()
            result = upload_domains.find_domain(conn, base_url_ex, subdomain_ex)
            if len(result) == 0 and subdomain_ex != '':
                result = upload_domains.find_domain(conn, base_url_ex, '*')
            # do something with the domains of the external links?
    #update done field for item
    # conn=analyze_api_database.connect_db()
    # cursor=conn.cursor()
    # sql='update items set status="done" where id={itemcode}'.format(itemcode=item['itemcode'])
    # cursor.execute(sql)
    # conn.commit()
    #conn.close()
    analyze_api_database.update_item_status(item['itemcode'],'done')
    #check if all items are done downloading
    print('checking if done: ',job_id)
    done=analyze_api_database.check_job_status(job_id)
    if skipped:
        print('skipped item done')
        done=True
    print('done status: ' ,done)
    #if all items are done, run grep analysis, image analysis on entire job, then update the job's end
    if done:
        print('going to analyze grep results job end')
        analyze_grep_results(job_id)
        print('getting images and videos for job_id ',job_id)
        images, videos = get_site_images_and_videos(job_id)
        print('total amount of images external and internal: ', len(images))
        print('going to analyze images for job_id ', job_id)
        analyze_site_images(job_id, images)
        print('goin to analyze videos for job_id ', job_id)
        analyze_site_videos(job_id, videos)
        print('going to update results for job_id ', job_id)
        update_results(job_id,data)
        print('going to update job end for job_id: ',job_id)
        analyze_api_database.update_job_info(job_id)
        shutil.rmtree(job_id,ignore_errors=True)
    conn.close()
    if skipped:
        results=analyze_api_database.get_item_results(job_id,item["itemcode"])
        return results
    else:
        return



async def check_sites_task(data, job_id, process_amount):
    # Check sites given
    timer = Timer(text=f"Sites Task elapsed time: {{:.1f}}")
    # job_id = str(uuid.uuid1())
    timer.start()
    result = []
    #print('got into check sites with this data:' ,data)
    # count = 0
    # for item in data:
    #     if count%process_amount==0:
    #         check_a_site.delay(item,job_id,data)
    #     else:
    #         itemcode = item['itemcode']
    #         analyze_api_database.update_item_status(itemcode, 'skipped')
    #     count+=1
    # count = 0
    # for item in data:
    #     if count%process_amount!=0:
    #         itemcode = item['itemcode']
    #         analyze_api_database.update_item_status(itemcode, 'skipped')
    #     count+=1
    # count=0
    # for item in data:
    #     if count%process_amount==0:
    #         check_a_site.delay(item,job_id,data)
    #     count+=1

    for item in data:
        check_a_site(item,job_id,data)

    timer.stop()
    return



async def check_texts_task(data, job_id, process_amount):
    # Check texts given
    conn = analyze_api_database.connect_db()
    timer = Timer(text=f"Texts Task elapsed time: {{:.1f}}")
    timer.start()
    response = analyze_text(data)
    data_results = response.json()['conversation']
    #print("The results of the text analysis is: ", data_results)
    count = 0
    for item in data_results:
        itemcode = item['itemcode']
        if count % process_amount == 0:
            item_results = item['predictions']
            results = []
            sexual = 0
            for result in item_results:
                label = result['label']
                confidence = result['confidence']
                if label == 'insult' and confidence > .50:
                    results.append({"confidence": str(confidence), "label": label})
                elif label == 'hate' and confidence > .50:
                    results.append({"confidence": str(confidence), "label": label})
                elif label == 'sexting' and confidence > .50:
                    sexual = confidence
                elif label == "sexual_solicitation" and confidence > .50:
                    sexual = max(sexual, confidence)
                elif label == "profanity_defamation" and confidence > .50:
                    results.append({"confidence": str(confidence), "label": label})
            if sexual > 0:
                results.append({"confidence": str(sexual), "label": "sexual"})
            if len(item_results) == 0:
                errorcode = 400
            else:
                errorcode = 0
            analyze_api_database.save_item_results(conn, job_id, itemcode, errorcode, results)
        else:
            analyze_api_database.update_item_status(itemcode, 'skipped')
        count+=1
    timer.stop()
    conn.close()
    return

def process_analyze_mock_api_status(user, job_id, mock_data, mock_status_start):
    print('Generating Analysis Mock API', job_id,' for user', user)
    now = str(int(time.time()))
    datereceived = str(mock_status_start)
    result = {"status": "completed", "job_id": job_id,  'datereceived': datereceived, 'datecompleted': now,'results': [{'id': 'mock',
                            'images': [{'imageId': 1, 'imageName': 'image1.jpg', 'results': [{'label': 'sexual_activity', 'confidence': 0.80}], 'errorCode': 0},
                                       {'imageId': 2, 'imageName': 'image2.jpg', 'results': [{'label': 'nudity', 'confidence': 0.92}], 'errorCode': 0},
                                       #{'imageId': 3, 'imageName': 'image3.jpg', 'results': [{'label': 'erotica', 'confidence': 0.76 }], 'errorCode': 0},
                                       {'imageId': 4, 'imageName': 'image4.jpg', 'results': [{'label': 'clean', 'confidence': 0.67}], 'errorCode': 0},
                                       {'imageId': 5, 'imageName': 'image5.jpg', 'results': [{'label': 'sexual_activity', 'confidence': 0.80}, {'label': 'underage', 'confidence': 0.99}], 'errorCode': 0},
                                       {'imageId': 6, 'imageName': 'image6.jpg', 'results': [{'label': 'nudity', 'confidence': 0.92}, {'label': 'underage', 'confidence': 0.66}], 'errorCode': 0},
                                       #{'imageId': 7, 'imageName': 'image7.jpg', 'results': [{'label': 'erotica_underage', 'confidence': 0.76}], 'errorCode': 0},
                                       {'imageId': 8, 'imageName': 'image8.jpg', 'results': [{}], 'errorCode': 500}
                                       ],
                            'siteClassification': [{'url': 'www.pornhub.com', 'domain': True, 'results': [{'label': 'porn', 'confidence': 0.99}], 'errorCode': 0},
                                                    {'url': '1escorts.com.au', 'domain': True, 'results': [{'label': 'escort', 'confidence': 0.78}], 'errorCode': 0},
                                                    {'url': 'l1ght.com', 'domain': False, 'results': [{'label': 'clean', 'confidence': 0.87}], 'errorCode': 0},
                                                    {'url': 'xxx.xx', 'results': [{}], 'errorCode': 400}],
                            'texts':[#{'text': 'The kikes and niggers rape and pillage our women and must be destroyed!', 'results':[{'label': 'hate_speech', 'confidence': 0.99}],  'errorCode': 0 },
                                     {'text': 'go to hell you shitface', 'results':[{'label': 'insulting', 'confidence': 0.89}],  'errorCode': 0},
                                    {'text': 'show me some tits and ass', 'results':[{'label': 'sexual_material', 'confidence': 0.86}],  'errorCode': 0},
                                    {'text': 'hey buddy, wtf is up? ', 'results':[{'label': 'clean', 'confidence': 0.98}],  'errorCode': 0},
                                                                     {'text': 'fdscf', 'results': [
                                                                         {}],
                                                                      'errorCode': 300}]}]}

    if len(mock_data) > 0:
        result['customerData'] = mock_data
    return result

async def api_l1ght(job_id, total_images, total_urls, total_texts, process_amount):
    tasks = []
    max_images = len(total_images)
    max_urls = len(total_urls)
    max_texts = len(total_texts)
    if max_images>0:
        # Python 3.6
        tasks.append(asyncio.ensure_future(check_images_task(total_images, job_id, process_amount)))
    if max_texts>0:
        # Python 3.6
        tasks.append(asyncio.ensure_future(check_texts_task(total_texts, job_id, process_amount)))
    if max_urls>0:
        # Python 3.6
        tasks.append(asyncio.ensure_future(check_sites_task(total_urls, job_id, process_amount)))
    # Run the tasks
    with Timer(text="\nTotal elapsed time: {:.1f}"):
        for f in asyncio.as_completed(tasks):
            await f
    return

def process_analyze_prod_api(user, job_id, data, total_images, total_urls, total_texts, process_amount):
    print('Starting Analysis Prod API', job_id, ' for user', user)
    #This is only available in python 3.7
    print("Processing Items: ")
    # This is for Python 3.7 and later (1 line)
    #asyncio.run(api_l1ght(item))
    # This is for Python 3.6 (4 lines)
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(api_l1ght(job_id, total_images, total_urls, total_texts, process_amount))
    loop.close()
    #analyze_api_database.update_job_info(job_id)
    #shutil.rmtree(job_id, ignore_errors=True)
    return


def process_analyze_prod_api_status(user, job_id):
    job_info = analyze_api_database.get_job_info(job_id)
    results = analyze_api_database.get_job_results(job_id)
    result = {"status": "completed", "job_id": job_id,  'datereceived': job_info[0]['datereceived'], 'datecompleted': job_info[0]['end'],'results': results}
    if len(job_info[0]['customerdata']) > 0:
        result['customerData'] = job_info[0]['customerdata']
    return result


def process_skipped_items(job_id):
    conn=analyze_api_database.connect_db()
    cursor=conn.cursor()
    #get all skipped item
    sql="select * from items where job_id='{}' and status like '%skipped%'".format(job_id)
    cursor.execute(sql)
    results=cursor.fetchall()
    print('found these skipped items:' , results)
    #update skipped items status to pending
    # for result in results:
    #     analyze_api_database.update_item_status('pending',str(result['id']))
    for res in results:
        stat=analyze_api_database.get_item_status(str(res['id']))
        #print('this status should be "pending":',stat)
    sql="update items set status='pending' where job_id='{}' and status='skipped'".format(job_id)
    cursor.execute(sql)
    conn.commit()
    conn.close()
    #create data dict of urls, each one with "url","domain","itemcode"
    skipped_items = []
    for result in results:
        if result['status']=='skipped':
            skipped_items.append({"url":result["url"],"domain":result["domain"],"itemcode":result["id"]})
    #send it to check_a_site
    print('going to check sites with these items:', skipped_items)
    for item in skipped_items:
        check_a_site.delay(item,job_id,skipped_items,skipped=True)


def get_most_skipped_urls(num_of_urls):
    conn=analyze_api_database.connect_db()
    cursor=conn.cursor()
    sql="select url,count(url) from items where status='skipped' group by url order by count(url) desc limit {}".format(num_of_urls)
    cursor.execute(sql)
    results=cursor.fetchall()
    return results


def analyze_most_skipped_site(num_of_urls):
    skipped_sites=get_most_skipped_urls(num_of_urls)
    print('got these top skipped sites:',skipped_sites)
    for url in skipped_sites:
        print("working on this skipped url:",url)
        sample_job_id,sample_item_id,sample_domain=analyze_api_database.get_sample_job(url["url"])
        results_before_analyzing=analyze_api_database.get_item_results(sample_job_id,sample_item_id)
        print('item has these results before analyzing:', results_before_analyzing)
        print('got these sample job_id,item_id,doman:',sample_job_id,sample_item_id,sample_domain)
        item={"url":url["url"],"domain":sample_domain,"itemcode":sample_item_id}
        analyze_api_database.update_item_status(sample_item_id,"pending")
        #item_classification=check_a_site.delay(item,sample_job_id,[item],skipped=True)
        item_classification=check_a_site(item,sample_job_id,[item],skipped=True)
        item_classification,errorcode=analyze_api_database.get_sample_results(sample_job_id,sample_item_id)
        print('got this classification for url:',item_classification,url['url'])
        analyze_api_database.update_skipped_url(url["url"],item_classification,errorcode)
        print('updated all items with this url',url['url'])
        results_after_analyzing = analyze_api_database.get_item_results(sample_job_id, sample_item_id)
        print('item has these results after analyzing:', results_after_analyzing)
    print('finished updating skipped sites')
    return





def increase_hit(domain):
    conn=analyze_api_database.connect_db()
    cursor=conn.cursor()
    sql="update exempt set hits=hits+1 where domain='{}'".format(domain)
    cursor.execute(sql)
    conn.commit()
    conn.close()

def clear_exempt_domains(total_urls,job_id,exempt_dict):
    new_total_urls=[]
    conn = analyze_api_database.connect_db()
    for url in total_urls:
        subdomain,base_url=upload_domains.extract_domain(url['url'])
        url_type=get_url_type(url['url'])
        if base_url in exempt_dict.keys():
            status=exempt_dict[base_url]
            result=[{'label':'{}'.format(status),'confidence': 1}]
            analyze_api_database.update_item_status(url['itemcode'],'done')
            analyze_api_database.save_item_results(conn,job_id,url['itemcode'],0,result)
            increase_hit(base_url)
        elif url_type in ['page','domain','video']:
            analyze_api_database.update_item_status(url['itemcode'], 'skipped')
        else:
            new_total_urls.append(url)
    conn.close()
    return new_total_urls