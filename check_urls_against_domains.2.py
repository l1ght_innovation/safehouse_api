import pymysql
import pymysql.cursors
import time
import datetime
import boto3
import logging
from botocore.exceptions import ClientError
import requests
import pandas as pd
import urllib.parse
import os
import json
import hashlib
import tldextract

rds_host = "localhost"
rds_name = 'admin'
rds_password = 'Antitoxin613'
rds_db_name = 'webscrapper'


def connect_db():
    conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def get_all_jobs(conn, startdate, enddate):
    cursor = conn.cursor()
    cursor.execute("select job_id from jobs where not clean and start > '"+startdate+"' and start < '"+enddate+"'")
    result = cursor.fetchall()
    resultdf = pd.DataFrame(result)
    x = resultdf['job_id'].to_list()
    return tuple(x)

def get_all_toxic_domains(conn):
    cursor = conn.cursor()
    cursor.execute("select * from domains where not clean")
    result = cursor.fetchall()
    resultdf = pd.DataFrame(result)
    return resultdf['domain'].to_list()

def get_all_items(conn,jobs):
    cursor = conn.cursor()
    cursor.execute("select distinct url from items where item_type = 'urls' and job_id in '"+str(jobs)+"'")
    result = cursor.fetchall()
    resultdf = pd.DataFrame(result)
    return resultdf

def get_all_items_revised(conn):
    cursor = conn.cursor()
    cursor.execute('select distinct url from items where item_type = "urls" and job_id in (select job_id from jobs where datereceived > unix_timestamp("2020-04-07 00:00:00"));')
    result = cursor.fetchall()
    print("Found : ", len(result), "records")
    resultdf = pd.DataFrame(result)
    return resultdf

def check_site(url, toxic):
    domain = tldextract.extract(url).registered_domain
    if domain in toxic:
        return 1
    elif 'escort' in domain:
        return 1
    elif 'porn' in domain:
        return 1
    elif 'pussy' in domain:
        return 1
    elif 'dicks' in domain:
        return 1
    elif 'anal' in domain:
        return 1
    elif 'sex' in domain:
        return 1
    else:
        return 0

def check_url_for_picture(url):
    if url.endswith('.jpg'):
        return 1
    elif url.endswith('.jpeg'):
        return 1
    elif url.endswith('.gif'):
        return 1
    elif url.endswith('.bmp'):
        return 1
    elif url.endswith('.webp'):
        return 1
    elif url.endswith('.png'):
        return 1
    elif '.jpg' in url:
        return 1
    elif '.jpeg' in url:
        return 1
    elif '.gif' in url:
        return 1
    elif '.bmp' in url:
        return 1
    elif '.webp' in url:
        return 1
    elif '.png' in url:
        return 1
    else:
        return 0

def check_url_for_movie(url):
    if url.endswith('.avi'):
        return 1
    elif url.endswith('.mov'):
        return 1
    elif url.endswith('.mp4'):
        return 1
    elif '.avi' in url:
        return 1
    elif '.mov' in url:
        return 1
    elif '.mp4' in url:
        return 1
    else:
        return 0

def check_link_for_url(url, fqdn):
    if url.strip() == fqdn.strip():
        return 1
    elif url.strip() == "http://"+fqdn.strip():
        return 1
    elif url.strip() == "https://"+fqdn.strip():
        return 1
    else:
        return 0

def check_domain(conn, registered_domain):
    cursor = conn.cursor()
    #cursor.execute("select * from domains where domain='"+registered_domain+"' and subdomain = '*' limit 1")
    cursor.execute("select * from domains where domain='" + registered_domain + "' limit 1")
    result = cursor.fetchall()
    if len(result) == 0:
        return -1
    elif result[0]['clean'] == 0:
        return 1
    else:
        return 0


conn = connect_db()
startdate = '2020-03-31'
enddate = '2020-04-02'
#toxic = get_all_toxic_domains(conn)
#jobs = get_all_jobs(conn, startdate, enddate)
#resultdf = get_all_items(conn, jobs)
resultdf = get_all_items_revised(conn)
records = []
for index, row in resultdf.iterrows():
    clean = 0
    toxic = 0
    unknown = 0
    page = 0
    file = 0
    url = row['url']
    item_type = 'urls'
    pic = check_url_for_picture(url)
    video = check_url_for_movie(url)
    domain = tldextract.extract(url).fqdn
    registered_domain = tldextract.extract(url).registered_domain
    check_in_known_domain = check_domain(conn, registered_domain)
    if check_in_known_domain == 0:
        clean = 1
    elif check_in_known_domain == 1:
        toxic = 1
    else:
        unknown = 0
    full = check_link_for_url(url, domain)
    if full != 1:
        if '.zip' in url:
            file = 1
        elif '.mp3' in url:
            file = 1
        else:
            page = 1
    record = {'url': url, 'item_type': item_type, 'clean': clean, 'toxic': toxic, 'full': full,
              'unknown_url': unknown, 'pic': pic, 'page': page, 'invalid_file': file, 'video': video}
    print(record)
    records.append(record)
records_df = pd.DataFrame(records)
records_df.to_csv('results-trial.csv', index=False)
#resultdf['porn'] = resultdf.apply(lambda row: check_site(row['url'], toxic),axis=1)
#resultdf['pic'] = resultdf.apply(lambda row: check_url(row['url']), axis=1)
#resultdf.to_csv('results-'+startdate+'-'+enddate+'.csv')
conn.close()

