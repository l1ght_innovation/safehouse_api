import pymysql
import pymysql.cursors
import time
import datetime
import boto3
import logging
from botocore.exceptions import ClientError
import requests
import pandas as pd
import urllib.parse
import os
import json
import hashlib
import analyze_api_database
import tldextract
import analyze_api_async
import pprint
import upload_domains
import ast


rds_host = "localhost"
rds_name = 'root'
rds_password = ''
rds_db_name = 'webscrapper'

error_file='image_errors.log'
err_f=open(error_file,'a+')

not_class_file='not_class.log'
nclass_f=open(not_class_file,'a+')

def connect_db():
    conn=analyze_api_database.connect_db()
    #conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def get_jobs_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    for i in range (3,31):
        day=str(i)
        if len(day)==1:
            day='0'+day
        sql="select count(*) from jobs where from_unixtime(datereceived) like '2020-03-{}%' and client_id=2".format(day)
        cursor.execute(sql)
        reslut=cursor.fetchall()
        jobs=reslut[0]['count(*)']
        print('march',day, jobs)


def get_urls_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    for i in range (3,31):
        day=str(i)
        if len(day)==1:
            day='0'+day
        sql="select count(*) from items where item_type='urls' and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-03-{}%' and client_id=2)".format(day)
        cursor.execute(sql)
        res=cursor.fetchall()
        urls=res[0]['count(*)']
        print('march',day,urls)


def get_unique_urls_per_day():
    conn = connect_db()
    cursor = conn.cursor()
    for i in range(3, 31):
        day = str(i)
        if len(day) == 1:
            day = '0' + day
        sql = "select count(distinct url) from items where item_type='urls' and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-03-{}%' and client_id=2)".format(
            day)
        cursor.execute(sql)
        res = cursor.fetchall()
        urls = res[0]['count(distinct url)']
        print('march', day, urls)

def get_num_of_images():
    conn = connect_db()
    cursor = conn.cursor()
    for i in range(3, 31):
        day = str(i)
        if len(day) == 1:
            day = '0' + day

        sql="select count(*) from webscrapper.items where item_type = 'urls' and (url like '%.gif' or url like '%.png' or url like '%.jpeg' or url like '%.jpg' or url like '%.webp' or url like '%.bmp') and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-03-{}%' and client_id=2)".format(day)
        cursor.execute(sql)
        res = cursor.fetchall()
        img = res[0]['count(*)']
        print('march', day, img)


def get_num_of_porn_sites():
    conn = connect_db()
    cursor = conn.cursor()
    for i in range(3, 31):
        day = str(i)
        if len(day) == 1:
            day = '0' + day
        sql="select count(*) from results where results like '%porn%' and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-03-{}%' and client_id=2) ".format(day)
        cursor.execute(sql)
        res = cursor.fetchall()
        porn = res[0]['count(*)']
        print('march', day, porn)


def get_num_of_escort_sites():
    conn = connect_db()
    cursor = conn.cursor()
    for i in range(3, 31):
        day = str(i)
        if len(day) == 1:
            day = '0' + day
        sql = "select count(*) from results where results like '%escort%' and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-03-{}%' and client_id=2) ".format(
            day)
        cursor.execute(sql)
        res = cursor.fetchall()
        porn = res[0]['count(*)']
        print('march', day, porn)

def get_unique_images():
    conn = connect_db()
    cursor = conn.cursor()
    for i in range(3, 31):
        day = str(i)
        if len(day) == 1:
            day = '0' + day

        sql = "select count(distinct url) from webscrapper.items where item_type = 'urls' and (url like '%.gif' or url like '%.png' or url like '%.jpeg' or url like '%.jpg' or url like '%.webp' or url like '%.bmp') and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-03-{}%' and client_id=2)".format(
            day)
        cursor.execute(sql)
        res = cursor.fetchall()
        img = res[0]['count(distinct url)']
        print('march', day, img)


def get_processed_items_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    print('processed items:')
    for i in range(28,31):
        day=str(i)
        sql="select count(*) from items where status='done' and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%')".format(i)
        cursor.execute(sql)
        res=cursor.fetchall()
        processed_items=res[0]['count(*)']
        print('april',day,processed_items)

def get_skipped_items_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    print('skipped items:')
    for i in range(28,31):
        day=str(i)
        sql="select count(*) from items where status='skipped' and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%')".format(i)
        cursor.execute(sql)
        res=cursor.fetchall()
        processed_items=res[0]['count(*)']
        print('april',day,processed_items)


def get_processed_images_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    print('processed images:')
    for i in range(18,22):
        day=str(i)
        sql="select count(*) from items where status='done' and (url like '%.jpg%' or url like '%.jpeg%' or url like '%.png%' or url like '%.webp%' or url like '%.bmp%' or url like '%.png%' )and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%')".format(i)
        cursor.execute(sql)
        res=cursor.fetchall()
        processed_items=res[0]['count(*)']
        print('april',day,processed_items)


def get_skipped_images_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    print('skipped images:')
    for i in range(18,22):
        day=str(i)
        sql="select count(*) from items where status='skipped' and (url like '%.jpg%' or url like '%.jpeg%' or url like '%.png%' or url like '%.webp%' or url like '%.bmp%' or url like '%.png%' )and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%')".format(i)
        cursor.execute(sql)
        res=cursor.fetchall()
        processed_items=res[0]['count(*)']
        print('april',day,processed_items)


def get_processed_movies_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    print('processed movies:')
    for i in range(18,22):
        day=str(i)
        sql="select count(*) from items where status='done' and (url like '%.avi%' or url like '%.movg%' or url like '%.mp4%')and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%')".format(i)
        cursor.execute(sql)
        res=cursor.fetchall()
        processed_items=res[0]['count(*)']
        print('april',day,processed_items)


def get_skipped_movies_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    print('skipped movies:')
    for i in range(18,22):
        day=str(i)
        sql="select count(*) from items where status='skipped' and (url like '%.avi%' or url like '%.movg%' or url like '%.mp4%')and job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%')".format(i)
        cursor.execute(sql)
        res=cursor.fetchall()
        processed_items=res[0]['count(*)']
        print('april',day,processed_items)


def get_processed_domains_per_day():
    conn=connect_db()
    cursor=conn.cursor()
    print('processed domains:')
    for i in range(18,22):
        day=str(i)
        sql="select url from items where status='done' and  job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%') ".format(day)
        cursor.execute(sql)
        results=cursor.fetchall()
        domain_count=0
        for res in results:
            url=res['url']
            if url.startswith('http'):
                index=url.find('//')
                url=url[index+2:]
            if url[-1]=='/':
                url=url[:-1]
            url_details=tldextract.extract(url)
            if url==url_details.domain+'.'+url_details.suffix or url==url_details.subdomain+'.'+url_details.domain+'.'+url_details.suffix:
                domain_count+=1
        print('april', day, domain_count)


def get_skipped_domains_per_day():
    conn = connect_db()
    cursor = conn.cursor()
    print('skipped domains:')
    for i in range(18, 22):
        day = str(i)
        sql = "select url from items where status='skipped' and  job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%') ".format(day)
        cursor.execute(sql)
        results = cursor.fetchall()
        domain_count = 0
        for res in results:
            url = res['url']
            if url.startswith('http'):
                index = url.find('//')
                url = url[index + 2:]
            if url[-1] == '/':
                url = url[:-1]
            url_details = tldextract.extract(url)
            if url == url_details.domain + '.' + url_details.suffix or url == url_details.subdomain + '.' + url_details.domain + '.' + url_details.suffix:
                domain_count += 1
        print('april', day, domain_count)




def main_func(start,end):

    conn=connect_db()
    cursor=conn.cursor()
    for i in range(start,end):
        key = {
            "domain": {"done": {"toxic": 0, "clean": 0,"not_classified":0,"unknown":0,"no_results":0}, "skipped": 0},
            "image": {"done": {"toxic": 0, "clean": 0,"not_classified":0,"unknown":0,"no_results":0}, "skipped": 0},
            "video": {"done": {"toxic": 0, "clean": 0,"not_classified":0,"unknown":0,"no_results":0}, "skipped": 0},
            "page": {"done": {"toxic": 0, "clean": 0,"not_classified":0,"unknown":0,"no_results":0}, "skipped": 0}
        }
        day=str(i)
        if len(day)>2:
            day='0'+day
        sql="select job_id,url,id,status from items where job_id in (select job_id from jobs where from_unixtime(datereceived) like '2020-04-{}%')".format(day)
        cursor.execute(sql)
        results=cursor.fetchall()
        for res in results:
            try:
                url_type= analyze_api_async.get_url_type(res['url'])
            except:
                url_type='page'
            status=res['status']
            job_id=res['job_id']
            item_id=res['id']
            url=res['url']
            if status=='done':
                sql="select results from results where job_id='{}' and item_id={}".format(res['job_id'],res['id'])
                cursor.execute(sql)
                res_res=cursor.fetchall()
                if len(res_res)>0:
                    #res_dict=json.loads(res[0]['results'])
                    # res_dict=ast.literal_eval(res[0]['results'])
                    # pprint.pprint(res_dict)
                    if any(porn_indicator in res_res[0]['results'] for porn_indicator in ['porn','escort','nudity','sexual_activity']):
                        url_class='toxic'
                        key[url_type]['done']['toxic']+=1
                    elif 'clean' in res_res[0]['results']:
                        url_class='clean'
                        key[url_type]['done']['clean']+=1
                    elif 'could not analyze' in res_res[0]['results']:
                        key[url_type]['done']['not_classified']+=1
                    else:
                        key[url_type]['done']['unknown']+=1
                        line=job_id+','+str(item_id)+','+url+','+res_res[0]['results']+'\n'
                        err_f.write(line)
                    # if 'porn' in res[0]['results'] or 'escort' in res[0]['results']:
                    #     url_class='toxic'
                    #     key[url_type]['done']['toxic']+=1
                    # else:
                    #     url_class='clean'
                    #     key[url_type]['done']['clean'] += 1
                else:
                    key[url_type]['done']['no_results']+=1
                    line = job_id + ',' + str(item_id) + ',' + url  + '\n'
                    nclass_f.write(line)
            else:
                key[url_type]['skipped']+=1
        print('apri',day)
        pprint.pprint(key)


def get_toxic_images():
    exempt_dict = analyze_api_database.create_exempt_dict()
    conn=analyze_api_database.connect_db()
    cursor=conn.cursor()
    sql="select items.job_id,items.id,items.url,results.results from items,results  where results.job_id=items.job_id and results.item_id=items.id and (results.results like '%porn%' or results.results like '%escort%') and items.job_id in (select job_id from jobs where from_unixtime(datereceived)>'2020-04-20 10:00')"
    cursor.execute(sql)
    results=cursor.fetchall()
    count=0
    for res in results:
        url=res['url']
        subdomain,base_url=upload_domains.extract_domain(url)
        type=analyze_api_async.get_url_type(url)
        if type=='image' and not any(porn_indicator in url.lower() for porn_indicator in ['porn', 'xnxx', 'xvideos']) and  base_url not in exempt_dict.keys():
            print(url)
            count+=1
    print('total urls:' ,count)
    conn.close()


if __name__ =="__main__":
    get_processed_items_per_day()
    get_skipped_items_per_day()
    main_func(28,31)