import os
import base64

INPUT_DIRECTORY='img/'
ALLOWED_EXT=['jpg','png','bmp']

def get_images(file_list):
    image_list=[]
    for file in file_list:
        file_extension=file.split('.')[-1]
        if file_extension in ALLOWED_EXT:
            image_list.append(file)
    return image_list


def save_local():
    image_dir = INPUT_DIRECTORY
    images = os.listdir(image_dir)
    filtered_images = get_images(images)
    for image in filtered_images:
        with open(image_dir + image, "rb") as img_file:
            byte_content = img_file.read()
            base64_str=base64.b64encode(byte_content)
            print(base64_str)
            with open(INPUT_DIRECTORY+'base64/'+image,'wb') as f:
                f.write(base64_str)

if __name__=="__main__":
    save_local()