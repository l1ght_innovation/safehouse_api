import os
import json
import requests
import pandas as pd
from glob import glob


def analyze_file_picture(images_dict):
#   Analyze the list of images (FILES) that are sent
    image_classification_url = 'http://0.0.0.0:7029/api/classify'
    headers = {'content-type': 'application/json'}
    body = {"images": images_dict}
    jsonbody = json.dumps(body)
    response = requests.post(image_classification_url, data=jsonbody, headers=headers)
    return response

def get_site_images_and_videos(job_id):
    images = []
    videos = []
    for root, dir, files in os.walk(job_id):
            for file in files:
                ext = file.split('.')[-1].lower()
                if ext in ['jpg', 'jpeg', 'gif', 'webp', 'png', 'bmp']:
                    images.append({"filename": os.path.join(root, file)})
                if ext in ['mp4', 'avi']:
                    videos.append(os.path.join(root, file))
    return images, videos

def analyze_results(json_results):
    results = []
    for item in json_results: 
        nudity = 0
        erotica = 0 
        sexual_activity = 0
        clean = 0
        underage = 0 
        value = item["predictions"][0]["value"].strip().lower()
        confidence = item["predictions"][0]["confidence"]
        print(value, confidence)
        if value == "neutral":
            clean = confidence
        elif value == "erotica":
            erotica = confidence
        elif value == "sexual activity":
            sexual_activity = confidence 
        elif value == "nudity":
            nudity = confidence
        for i in item["predictions"][1]["value"]:
            if i["value"]!="18_and_above":
               underage = 1
        filename_full = item["filename"].split('/')
        category = filename_full[-2]
        filename = filename_full[-1]
        item_result = {"category": category, "filename": filename, "underage": underage, "sa": sexual_activity, "nudity": nudity, "erotica": erotica, "clean": clean}
        results.append(item_result)
    return results 

images, videos = get_site_images_and_videos('/home/centos/analyze_api/api_pics')
response = analyze_file_picture(images)
#print(response.json())
results = analyze_results(response.json()['images'])
df = pd.DataFrame(results)
df.to_csv("results.csv")
