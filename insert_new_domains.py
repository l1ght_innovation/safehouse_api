import upload_domains
import pandas as pd
import analyze_api_database

if __name__=='__main__':
    clean_file='agtmp/clean_domains.csv'
    dirty_file='agtmp/dirty_domains.csv'
    conn=analyze_api_database.connect_db()

    dirty_df=pd.read_csv(dirty_file)
    for url in dirty_df['url']:
        subdomain,base_url=upload_domains.extract_domain(url)
        res=upload_domains.find_domain(conn,base_url,subdomain)
        if len(res)==0:
            upload_domains.save_new_domain(conn,base_url,subdomain,0,'[{''label'': ''porn'', ''confidence'': 1}]',1)
        else:
            upload_domains.update_existing_domain(conn,base_url,subdomain,0,'[{''label'': ''porn'', ''confidence'': 1}]',1)

    clean_df=pd.read_csv(clean_file)
    for url in clean_df['url']:
        subdomain,base_url=upload_domains.extract_domain(url)
        res = upload_domains.find_domain(conn, base_url, subdomain)
        if len(res) == 0:
            upload_domains.save_new_domain(conn,base_url,subdomain,1,'[]',1)
        else:
            upload_domains.update_existing_domain(conn, base_url, subdomain,1,'[]',1)
