import pymysql
import pymysql.cursors
import time
import datetime
import boto3
import logging
from botocore.exceptions import ClientError
import requests
import pandas as pd
import urllib.parse
import os
import json
import hashlib
import tldextract
import sys

rds_host = "localhost"
rds_name = 'admin'
rds_password = 'Antitoxin613'
rds_db_name = 'webscrapper'
#create table item_stats (id int auto_increment,  date varchar(10), url varchar(500), item_type varchar(10), clean int(1), toxic int(1), full int(1), unknown_url int(1), pic int(1), page int(1), invalid_file int(1), video int(1), constraint id_pk primary key (id));


def connect_db():
    conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def get_all_items_revised(conn, startdate, enddate):
    cursor = conn.cursor()
    cursor.execute('select url from items where item_type = "urls" and job_id in (select job_id from jobs where datereceived > unix_timestamp("'+startdate+' 00:00:00") and datereceived < unix_timestamp("'+enddate+' 00:00:00"));')
    result = cursor.fetchall()
    print("Found : ", len(result), "records")
    resultdf = pd.DataFrame(result)
    return resultdf

def check_site(url, toxic):
    domain = tldextract.extract(url).registered_domain
    if domain in toxic:
        return 1
    elif 'escort' in domain:
        return 1
    elif 'porn' in domain:
        return 1
    elif 'pussy' in domain:
        return 1
    elif 'dicks' in domain:
        return 1
    elif 'anal' in domain:
        return 1
    elif 'sex' in domain:
        return 1
    else:
        return 0

def check_url_for_picture(url):
    if url.endswith('.jpg'):
        return 1
    elif url.endswith('.jpeg'):
        return 1
    elif url.endswith('.gif'):
        return 1
    elif url.endswith('.bmp'):
        return 1
    elif url.endswith('.webp'):
        return 1
    elif url.endswith('.png'):
        return 1
    elif '.jpg' in url:
        return 1
    elif '.jpeg' in url:
        return 1
    elif '.gif' in url:
        return 1
    elif '.bmp' in url:
        return 1
    elif '.webp' in url:
        return 1
    elif '.png' in url:
        return 1
    else:
        return 0

def check_url_for_movie(url):
    if url.endswith('.avi'):
        return 1
    elif url.endswith('.mov'):
        return 1
    elif url.endswith('.mp4'):
        return 1
    elif '.avi' in url:
        return 1
    elif '.mov' in url:
        return 1
    elif '.mp4' in url:
        return 1
    else:
        return 0

def check_link_for_url(url, fqdn):
    if url.strip() == fqdn.strip():
        return 1
    elif url.strip() == "http://"+fqdn.strip():
        return 1
    elif url.strip() == "https://"+fqdn.strip():
        return 1
    else:
        return 0

def check_domain(conn, registered_domain):
    cursor = conn.cursor()
    #cursor.execute("select * from domains where domain='"+registered_domain+"' and subdomain = '*' limit 1")
    cursor.execute("select * from domains where domain='" + registered_domain + "' limit 1")
    result = cursor.fetchall()
    if len(result) == 0:
        return -1
    elif result[0]['clean'] == 0:
        return 1
    else:
        return 0

def insert_record_stats(conn, startdate, url, item_type, clean, toxic, full, unknown, pic, page, file, video):
    cursor = conn.cursor()
    sql = "insert into item_stats (date, url, item_type, clean, toxic, full, unknown_url, pic, page, invalid_file, video) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (startdate, url, item_type, clean, toxic, full, unknown, pic, page, file, video)
    cursor.execute(sql, val)
    conn.commit()
    return

def main(startdate, enddate):
    print("Checking for dates between:", startdate, enddate)
    conn = connect_db()
    resultdf = get_all_items_revised(conn, startdate,enddate)
    total_count = 0 
    total_pic = 0 
    total_movie = 0 
    for index, row in resultdf.iterrows():
        clean = 0
        toxic = 0
        unknown = 0
        page = 0
        file = 0
        url = row['url']
        item_type = 'urls'
        pic = check_url_for_picture(url)
        video = check_url_for_movie(url)
        total_count +=1
        if pic: 
            total_pic +=1
        if video:
            total_movie +=1
    conn.close()
    print("Checking for dates between:", startdate, enddate)
    print("Total count: ", total_count)
    print("Total pic: ", total_pic)
    print("Total movie: ", total_movie)

if __name__ == "__main__":
    if len(sys.argv)>1:
        main(sys.argv[1],sys.argv[2])
