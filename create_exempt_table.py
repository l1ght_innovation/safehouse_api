import pandas as pd
import analyze_api_database
import pprint
import upload_domains

INPUT_FILE='toxic_moderated_domains.csv'

def insert_to_table():
    conn=analyze_api_database.connect_db()
    cursor=conn.cursor()
    df=pd.read_csv(INPUT_FILE,sep='\t')
    for i in range (len(df['domain'])):
        domain=df.at[i,'domain']
        status=df.at[i,'labels']
        if 'porn' in str(status):
            status='porn'
        elif 'escort' in str(status):
            status='escort'
        else:
            status='unknown'
        sql =  """insert into exempt (domain,status,hits) values ("{}","{}",{})""".format(domain,status,0)
        print(sql)
        cursor.execute(sql)
        conn.commit()
        print('inserted')
    conn.close()

def test():
    key={}
    conn=analyze_api_database.connect_db()
    cursor=conn.cursor()
    sql="select * from exempt"
    cursor.execute(sql)
    results=cursor.fetchall()
    for res in results:
        key[res['domain']]=res['status']
    pprint.pprint(key)

    conn.close()


def insert_clean():
    conn=analyze_api_database.connect_db()
    cursor=conn.cursor()
    with open ('clean_domains.txt') as f:
        lines=f.readlines()
        for line in lines:
            url=line.strip()
            status='clean'
            sql="insert into exempt (domain,status,hits) values ('{}','{}',0)".format(url,status)
            cursor.execute(sql)
            conn.commit()
    conn.close()

def check_if_clean_in_domains_table():
    conn = analyze_api_database.connect_db()
    cursor = conn.cursor()
    with open('clean_domains.txt') as f:
        lines = f.readlines()
        for line in lines:
            url=line.strip()
            sql="select * from domains where domain='{}' and subdomain='*'".format(url)
            cursor.execute(sql)
            res=cursor.fetchall()
            if len(res)==0:
                conn2=analyze_api_database.connect_db()
                upload_domains.save_new_domain(conn2,url,'*',1,[{'label': 'clean', 'confidence':1}],1)
                print(url,res)
                conn2.close()
    conn.close()


if __name__=='__main__':
    check_if_clean_in_domains_table()