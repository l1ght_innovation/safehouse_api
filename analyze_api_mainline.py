from flask import Flask, flash, redirect, render_template, request, session, abort, send_file, g, jsonify
import uuid
#import analyze_api_database
#import analyze_api_async
import analyze_api_database
import analyze_api_async
import time
mock_status_progress = 0
mock_status_start = 0
mock_status_end = 0
mock_data = {}
app = Flask(__name__)
exempt_dict=analyze_api_database.create_exempt_dict()

@app.route('/api/mock/v1/analyze', methods=['POST'])
def mock_analyze_api():
    global mock_status_start, mock_status_end, mock_data
    mock_status_start = int(time.time())
    mock_status_end = mock_status_start + 15
    job_id = str(uuid.uuid1())
    data_json  = request.get_json()
    if 'data' not in data_json:
        return jsonify({"message": "Missing Data"}), 500
    elif 'Authentication' not in request.headers:
        return jsonify({"message": "Missing Authentication"}), 400
    else:
        user = analyze_api_database.check_user(request.headers.get('Authentication'))
        if user == -1:
            return jsonify({"message": "Invalid Authentication"}), 400
        else:
            if 'customerData' in data_json:
                mock_data = data_json['customerData']
            else:
                mock_data = {}
            result = {"job_id": job_id, "status": "in progress"}
        return jsonify(result), 200

@app.route('/api/mock/v1/status', methods=['POST'])
def mock_status_api():
    global mock_data, mock_status_start, mock_status_end, mock_status_progress
    data_json  = request.get_json()
    mock_status_progress=int(time.time())
    if 'job_id' not in data_json:
        return jsonify({"message": "Missing Job"}), 500
    elif 'Authentication' not in request.headers:
        return jsonify({"message": "Missing Authentication"}), 400
    else:
        user = analyze_api_database.check_user(request.headers.get('Authentication'))
        if user == -1:
            return jsonify({"message": "Invalid Authentication"}), 400
        job_id = data_json['job_id']
        #celery_process_analyze_api.delay(user, job_id, data_json)
        if mock_status_progress<mock_status_end:
            result = {"job_id": job_id, "status": "in progress"}
        else:
            result = analyze_api_async.process_analyze_mock_api_status(user, job_id, mock_data, mock_status_start)
        return jsonify(result), 200


@app.route('/api/prod/v1/analyze', methods=['POST'])
def analyze_api():
    job_id = str(uuid.uuid1())
    data_json  = request.get_json()
    if 'data' not in data_json:
        return jsonify({"status": "Error", "message": "Missing Data"}), 500
    elif 'Authentication' not in request.headers:
        return jsonify({"status": "Error", "message": "Missing Authentication"}), 400
    else:
        user = request.headers.get('Authentication')
        client = analyze_api_database.check_user(user)
        if client == -1:
            return jsonify({"message": "Invalid Authentication"}), 400
        total_images, total_urls, total_text = analyze_api_database.save_job_info(job_id,user, client, data_json)
        total_urls=analyze_api_async.clear_exempt_domains(total_urls,job_id,exempt_dict)
        if len(total_images)==0 and len(total_text)==0 and len(total_urls)==0:
            analyze_api_database.update_job_info(job_id)
        # if client==2:
        #     analyze_api_database.terminate_job(job_id)
        #     result={"job_id":job_id,"status":"not proccessed due to maintenance"}
        #else:
            # analyze_this_job=analyze_api_database.check_quota()
            # if analyze_this_job:
            #     analyze_api_async.celery_process_analyze_prod_start.delay(user, job_id, data_json, total_images, total_urls, total_text)
            #     result = {"job_id": job_id, "status": "in progress"}
            # else:
            #     analyze_api_database.terminate_job(job_id)
            #     result= {"job_id":job_id,"status":"request exceeded analysis quota"}
        analyze_api_async.celery_process_analyze_prod_start.delay(user, job_id, data_json, total_images, total_urls, total_text)
        result = {"job_id": job_id, "status": "in progress"}
        return jsonify(result), 200

@app.route('/api/prod/v1/status', methods=['POST'])
def status_api():
    data_json  = request.get_json()
    if 'job_id' not in data_json:
        return jsonify({"message": "Missing Job"}), 500
    elif 'Authentication' not in request.headers:
        return jsonify({"message": "Missing Authentication"}), 400
    else:
        client = analyze_api_database.check_user(request.headers.get('Authentication'))
        if client == -1:
            return jsonify({"message": "Invalid Authentication"}), 400
        job_id = data_json['job_id']
        status, images, urls, texts = analyze_api_database.check_job(job_id, client)
        if status == -1:
            result = {"job_id": job_id, "message": "Invalid Job-Id"}
        elif status == -9:
            result = {"job_id": job_id, "message": "Restricted Job-Id"}
        print('getting job status')

        end_time=analyze_api_database.get_job_end_time(job_id)
        if end_time == '0000-00-00 00:00:00' :
            result = {"job_id": job_id, "message": "in progress", "total_images": images, "total_sites": urls, "total_texts": texts}
        else:
            # This is a hack to stop sending results:
            # if request.headers.get('Authentication') != 'e5b60f39183c3e57c94c504784077fe4':
            #     result = {"job_id": job_id, "message": "completed - results disabled", "total_images": images, "total_sites": urls,
            #      "total_texts": texts}
            # else:
            result = analyze_api_async.process_analyze_prod_api_status(client, job_id)
        return jsonify(result), 200

@app.route('/api/prod/v1/process_skipped_items',methods=['POST'])
def process_skipped_items():
    data_json = request.get_json()
    if 'job_id' not in data_json:
        return jsonify({"message": "Missing Job"}), 500
    elif 'Authentication' not in request.headers:
        return jsonify({"message": "Missing Authentication"}), 400
    else:
        client = analyze_api_database.check_user(request.headers.get('Authentication'))
        if client == -1:
            return jsonify({"message": "Invalid Authentication"}), 400
        job_id = data_json['job_id']
        status, images, urls, texts = analyze_api_database.check_job(job_id, client)
        if status == -1:
            result = {"job_id": job_id, "message": "Invalid Job-Id"}
        else:
            analyze_api_async.process_skipped_items(job_id)
            result={"job_id": job_id, "status": "in progress"}
        return jsonify(result),200


@app.route('/api/prod/v1/process_most_skipped_items',methods=['POST'])
def process_most_skipped_items():
    data_json = request.get_json()
    if 'Authentication' not in request.headers:
        return jsonify({"message": "Missing Authentication"}), 400
    else:
        client = analyze_api_database.check_user(request.headers.get('Authentication'))
        if client == -1:
            return jsonify({"message": "Invalid Authentication"}), 400
        else:
            analyze_api_async.analyze_most_skipped_site(3)
            result={"messege":"analyzing most skipped sites"}
        return jsonify(result),200


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=4000)
