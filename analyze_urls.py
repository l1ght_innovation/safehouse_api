import tldextract
import pandas as pd
import numpy



def check_url(original, redirect):
    print('check:',original, redirect)
    if redirect!=redirect or original!=original:
        return False
    o = tldextract.extract(original)
    r = tldextract.extract(redirect)

    if o.domain == r.domain:
        if o.suffix == r.suffix:
            return True
    return False


def check_url2(original, redirect):
    print('check:',original, redirect)
    if redirect!=redirect or original!=original:
        return False
    o = tldextract.extract(original)
    r = tldextract.extract(redirect)

    if o.domain == r.domain:
        if o.suffix == r.suffix:
            return True
    return False

#df = pd.read_excel('/Users/johndoe/Downloads/heziweek8.xlsx')
df = pd.read_csv('/Users/johndoe/Downloads/week8_spider_results_redirects.csv')

df['proper_result'] = df.apply(lambda row: check_url2(row[0], row[2]), axis=1)

#df.to_excel('/Users/johndoe/Downloads/heziweek8-fixed.xlsx')
df.to_excel('/Users/johndoe/Downloads/week8_spider_results_redirects_fixed.xlsx')